# mysite/routing.py
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url
from viajes.consumers import PedidosConsumer
from users.consumers1 import ChatConsumer
from conductores.consumers import ConductorConsumer
from channels.security.websocket import AllowedHostsOriginValidator

websocket_urlpatterns = [
    url(r'^ws/pedidos/$', PedidosConsumer),
    url(r'^ws/mensajes/$', ChatConsumer),
    url(r'^ws/ubicaciones/$', ConductorConsumer),
]

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                websocket_urlpatterns
            )
        ),
    ),
})