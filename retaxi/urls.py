"""retaxi re_path Configuration

The `re_pathpatterns` list routes re_paths to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/re_paths/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a re_path to re_pathpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a re_path to re_pathpatterns:  path('', Home.as_view(), name='home')
Including another re_pathconf
    1. Import the include() function: from django.re_paths import include, path
    2. Add a url to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from direcciones.views import ruta, lugares, detalle_lugar, getinfodire
from users.views import getPasajero, getConductor, asociarConductor
from conductores.views import ConductorDetail, ConductorList, MovilList, MovilDetail, actualizar_ubicacion, \
    ConductorViewSet, set_movil, remove_movil, conductor_msg_token, mapaMoviles
from mensajes.views import EnviarMensajeView
from pasajeros.views import PasajeroList, PasajeroDetail, pasajero_msg_token, pasajero_data_udt
from tarifas.views import TarifaDetail, TarifaList, prueba_calcular
from viajes.views import AceptarViajeView, PedirRemisView, ViajeListConductor, ViajeListPasajero, CancelarViajeView, iniciarViaje, finalizarViaje, \
    EnviarPosicion, puntuar_conductor, PedidosList, ViajeDetail, cambiarZona, cambiarEstado, PedidosFinalizados, cancelarPedido, editarPedido, editarZona, DetallePedido, asignarZona,cambiar_fechaAsignacion, quitarZona, pedidoChat, NuevoViajeList
from users.views import chat, ver_mensajes, enviarMensaje
from django.contrib.auth import views as auth_views
from django.conf import settings
from users.views import PasajeroLoginView, ConductorLoginView
from rviajes.views import RViajesList, RulesList
from zona.views import mapaZonas
from .views import mapa

urlpatterns = [
    path('grappelli/', include('grappelli.urls')), # grappelli urls
    #path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('auth/', include('rest_framework_social_oauth2.urls')),

    re_path('login/', auth_views.LoginView.as_view(), {'template_name': "login.html"},name='login'),
    re_path('logout/', auth_views.logout_then_login, name='logout'),

    re_path(r'^chat/$', view=chat, name='chat'),
    re_path(r'^mensajes/(?P<pk>[0-9]+)/', view=ver_mensajes, name='ver_mensajes'),

    re_path(r'^conductores/$', view=ConductorList.as_view()),
    re_path(r'^conductor/(?P<pk>\d+)/$',view=ConductorDetail.as_view()),
    re_path(r'^direcciones/ruta/([^/]+)/([^/]+)$', ruta),
    re_path(r'^direcciones/lugares/([^/]+)/([^/]+)$', lugares),    
    re_path(r'^direcciones/detalle_lugar/([^/]+)$', detalle_lugar),  
    re_path(r'^direcciones/getinfodire/([^/]+)$', getinfodire),   
    
    re_path(r'^get_pasajero/$', getPasajero),
    re_path(r'^get_conductor/$', getConductor),
    re_path(r'^asociar_conductor/$', asociarConductor),

    re_path(r'^login_pasajero/$', view=PasajeroLoginView.as_view(), name='pasajero_login'),
    re_path(r'^login_conductor/$', view=ConductorLoginView.as_view(), name='conductor_login'),

    re_path(r'^pasajeros/$', view=PasajeroList.as_view()),
    re_path(r'^pasajero/(?P<pk>\d+)/$', view=PasajeroDetail.as_view()),
    re_path(r'^moviles/$', view=MovilList.as_view()),
    re_path(r'^movil/(?P<pk>\d+)/$', view=MovilDetail.as_view()),
    re_path(r'^tarifas/$', view=TarifaList.as_view()),
    re_path(r'^tarifa/(?P<pk>\d+)/$', view=TarifaDetail.as_view()),
    re_path(r'^viajes_pasajero/$', ViajeListPasajero),
    re_path(r'^viajes_conductor/$', ViajeListConductor),
    re_path(r'^viaje/(?P<pk>\d+)/$', view=ViajeDetail.as_view()),
    re_path(r'^pedir_remis/$', view=PedirRemisView.as_view()),
    re_path(r'^aceptar_viaje/$', view=AceptarViajeView.as_view()),
    re_path(r'^cancelar_viaje/$', view=CancelarViajeView.as_view()),
    re_path(r'^envia_posicion/$', view=EnviarPosicion.as_view()),
    re_path(r'^enviar_msj/$', view=EnviarMensajeView.as_view()),
    re_path(r'^iniciar_viaje/$', iniciarViaje),
    re_path(r'^finalizar_viaje/$', finalizarViaje),
    re_path(r'^actualizar_ubicacion/$', actualizar_ubicacion),
    re_path(r'^calculartarifa/([^/]+)/([^/]+)$$', prueba_calcular),
    re_path(r'^puntuar_conductor/$', puntuar_conductor),
    re_path(r'^crear_movil/$', set_movil),
    re_path(r'^quitar_movil/$', remove_movil),
    re_path(r'^pasajero_msg_token/$', pasajero_msg_token),
    re_path(r'^pasajero_data_udt/$', pasajero_data_udt),
    re_path(r'^conductor_msg_token/$', conductor_msg_token),

    re_path(r'^pedidos/$', view=PedidosList, name='pedidos'),
    re_path(r'^finalizados/$', view=PedidosFinalizados.as_view(), name='finalizados'),
    re_path(r'^estado/', view=cambiarEstado, name='cambiar_estado'),
    re_path(r'^cancelar/', view=cancelarPedido, name='cancelar_pedido'),
    re_path(r'^asignarZona/', view=asignarZona, name='asignarZona'),
    re_path(r'^editarPedido/', view=editarPedido, name='editarPedido'),
    re_path(r'^editarZona/', view=editarZona, name='editarZona'),
    re_path(r'^cambiarZona/', view=cambiarZona, name='cambiarZona'),
    re_path(r'^quitarZona/', view=quitarZona, name='quitarZona'),
    re_path(r'^cambiar/', view=cambiar_fechaAsignacion, name='cambiar_fechaAsignacion'),
    re_path(r'^enviarMensaje/', view=enviarMensaje, name='enviarMensaje'),
    re_path(r'^pedidoChat/', view=pedidoChat, name='pedidoChat'),
    re_path(r'^detallePedido/(?P<pk>[0-9]+)/', view=DetallePedido.as_view(), name='detallePedido'),
    re_path(r'^mapaMoviles/', view=mapaMoviles, name='mapaMoviles'),

    re_path(r'^rreglas/', view=RulesList.as_view(), name='rreglas'),
    re_path(r'^rviajes/', view=RViajesList.as_view(), name='rviaje'),

    re_path(r'^nuevoViaje/$', view=NuevoViajeList, name='nuevoViaje'),
    re_path(r'^mapa/$', view=mapa, name='mapa'),

    re_path(r'^zonas/$', view=mapaZonas, name='zonas-list'),

]
