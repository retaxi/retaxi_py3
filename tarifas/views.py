import json

import decimal
from rest_framework import generics
from django.core import serializers
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from direcciones.views import distancia
from .serializers import TarifaSerializer
from .models import Tarifa

APP_NAME = 'tarifas'


class TarifaList(generics.ListAPIView):
    queryset = Tarifa.objects.all()
    serializer_class = TarifaSerializer

class TarifaDetail(generics.RetrieveAPIView):
    queryset = Tarifa.objects.all()
    serializer_class = TarifaSerializer


def CalcularTarifa(origen, destino):
    elements = distancia(origen, destino)['rows'][0]['elements'][0]
    if elements['status'] == 'OK': metros = elements['distance']['value']
    else: raise Exception('No se pudo calcular la distancia')

    tarifa = Tarifa.objects.get(first_name='Normal')

    if tarifa.metros_ficha == 0: tarifa.metros_ficha = 100
    total_cuadras = metros / tarifa.metros_ficha
    cuadras = metros // tarifa.metros_ficha
	
    precio_viaje = tarifa.bajada_de_bandera + ( cuadras * tarifa.ficha )

    serializer = serializers.serialize('json', [tarifa, ])
    struct = json.loads(serializer)
    struct[0]['fields']['precio_viaje'] = float(precio_viaje)
    struct[0]['fields']['metros'] = float(metros)
    struct[0]['fields']['cuadras'] = float(total_cuadras)

    #return Response(struct[0]['fields'])
    return struct[0]['fields']


@api_view(['GET'])
@permission_classes((AllowAny, ))
def prueba_calcular(request, origen, destino):
    return Response(json.dumps(CalcularTarifa(origen, destino)))