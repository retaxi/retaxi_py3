# -*- coding: utf-8 -*-
from django.db import models

RANGO_HORAS = range(0, 24)
DIAS = ((0, "Domingo"),
        (1, "Lunes"),
        (2, "Martes"),
        (3,"Miércoles"),
        (4, "Jueves"),
        (5, "Viernes"),
        (6, "Sábado"))

class Tarifa(models.Model):
    first_name = models.CharField(verbose_name='nombre', max_length=50, blank=False, null=False)
    bajada_de_bandera = models.DecimalField(verbose_name='bajada de bandera', max_digits=5, decimal_places=2, blank=False, null=False)
    ficha = models.DecimalField(verbose_name='ficha', max_digits=5, decimal_places=2, blank=False, null=False)
    minuto_espera = models.DecimalField(verbose_name='minuto de espera', max_digits=5, decimal_places=2, blank=False, null=False)
    adicional = models.DecimalField(verbose_name='adicional', max_digits=5, decimal_places=2, blank=False, null=False)
    metros_ficha = models.DecimalField(verbose_name='metros por ficha', max_digits=5, decimal_places=2, blank=False, null=False, default=0)

    #hora_desde = models.IntegerField(verbose_name='hora desde', choices=RANGO_HORAS, blank=False, null=False)
    #hora_hasta = models.IntegerField(verbose_name='hora hasta', choices=RANGO_HORAS, blank=False, null=False)

    class Meta:
        verbose_name = 'tarifa'
        verbose_name_plural = 'tarifas'