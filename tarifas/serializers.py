from django.conf.urls import url, include
from .models import Tarifa
from rest_framework import serializers

# Serializers define the API representation.
class TarifaSerializer(serializers.ModelSerializer):
    precio_viaje = serializers.IntegerField()
    cuadras = serializers.IntegerField()
    metros = serializers.IntegerField()

    class Meta:
        model = Tarifa
        fields = ('first_name', 'bajada_de_bandera', 'ficha', 'minuto_espera', 'adicional',
                  'precio_viaje', 'cuadras', 'metros')