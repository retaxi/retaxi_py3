from django.contrib import admin
from .models import Tarifa


# Register your models here.
class TarifaAdmin(admin.ModelAdmin):
    list_display = ['first_name', ]
    search_fields = ['first_name', ]

    class Meta:
        model = Tarifa

admin.site.register(Tarifa, TarifaAdmin)
