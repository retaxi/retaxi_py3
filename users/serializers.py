from rest_framework import serializers
from rest_social_auth.serializers import UserTokenSerializer

class UserThumbSerializer(UserTokenSerializer):
    social_thumb = serializers.SerializerMethodField()

    def get_social_thumb(self, obj):
        return obj.userthumb.social_thumb