from rest_framework.decorators import api_view

from pasajeros.models import Pasajero
from viajes.models import Viaje
from mensajes.models import Mensaje
from pasajeros.serializers import UserPasajeroSerializer
from conductores.models import Conductor
from conductores.serializers import UserConductorSerializer
from rest_social_auth.views import SocialTokenUserAuthView
from django.contrib.auth import login
from social_django.utils import psa, STORAGE
from django.http import HttpResponse
from social.utils import parse_qs
from social.strategies.utils import get_strategy
from requests.exceptions import HTTPError
from social.exceptions import AuthException
from django.conf import settings
from rest_framework.response import Response
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from pyfcm import FCMNotification
import logging
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.utils.safestring import mark_safe
from rest_framework.authtoken.models import Token

REDIRECT_URI = getattr(settings, 'REST_SOCIAL_OAUTH_REDIRECT_URI', '/')
l = logging.getLogger(__name__)

RESPUESTA = 'respuesta'
DNI_INVALIDO = '100'
CONDUCTOR_NO_REGISTRADO = '200'
USUARIO_INVALIDO = '300'
MESSAGE_TYPE = 'type'
MESSAGE_TYPE_CHAT = 'chat'

push_service = FCMNotification(api_key=settings.FIREBASE_API_KEY)

def load_strategy(request=None):
    return get_strategy('rest_social_auth.strategy.DRFStrategy', STORAGE, request)


@psa(REDIRECT_URI, load_strategy=load_strategy)
def decorate_request(request, backend):
    pass


class PasajeroLoginView(SocialTokenUserAuthView):
    serializer_class = UserPasajeroSerializer

    def do_login(self, backend, user):
        if user:
            try:
                pasajero = Pasajero.objects.get(user=user)
            except Pasajero.DoesNotExist:
                pasajero = Pasajero.objects.create(dni=str(user.id), first_name=user.first_name,
                                                   last_name=user.last_name, email=user.email, user=user)

            #login(self.request, user)
            serializer = self.get_serializer(instance=pasajero)
            return Response(serializer.data)

        return self.respond_error("Error de logueo")

    @method_decorator(never_cache)
    def post(self, request, *args, **kwargs):
        input_data = self.get_serializer_in_data()
        provider_name = self.get_provider_name(input_data)
        if not provider_name:
            return self.respond_error("Provider is not specified")
        self.set_input_data(request, input_data)
        decorate_request(request, provider_name)
        serializer_in = self.get_serializer_in(data=input_data)
        if self.oauth_v1() and request.backend.OAUTH_TOKEN_PARAMETER_NAME not in input_data:
            # oauth1 first stage (1st is get request_token, 2nd is get access_token)
            request_token = parse_qs(request.backend.set_unauthorized_token())
            return Response(request_token)
        serializer_in.is_valid(raise_exception=True)
        try:
            user = self.get_object()
        except (AuthException, HTTPError) as e:
            return self.respond_error(e)
        if isinstance(user, HttpResponse):  # An error happened and pipeline returned HttpResponse instead of user
            return user

        return self.do_login(request.backend, user)


class ConductorLoginView(SocialTokenUserAuthView):
    serializer_class = UserConductorSerializer

    @method_decorator(never_cache)
    def post(self, request, *args, **kwargs):
        input_data = self.get_serializer_in_data()
        provider_name = self.get_provider_name(input_data)
        if not provider_name:
            return self.respond_error("Provider is not specified")
        self.set_input_data(request, input_data)
        decorate_request(request, provider_name)
        serializer_in = self.get_serializer_in(data=input_data)
        if self.oauth_v1() and request.backend.OAUTH_TOKEN_PARAMETER_NAME not in input_data:
            # oauth1 first stage (1st is get request_token, 2nd is get access_token)
            request_token = parse_qs(request.backend.set_unauthorized_token())
            return Response(request_token)
        serializer_in.is_valid(raise_exception=True)
        try:
            user = self.get_object()
        except (AuthException, HTTPError) as e:
            return self.respond_error(e)
        if isinstance(user, HttpResponse):  # An error happened and pipeline returned HttpResponse instead of user
            return user

        return self.do_login(request.backend, user)

    def do_login(self, backend, user):

        if user:
            try:
                conductor = Conductor.objects.get(user=user)
            except Conductor.DoesNotExist:
                token, created = Token.objects.get_or_create(user=user)
                return Response({RESPUESTA: DNI_INVALIDO,
                                 'token': token.key})

            serializer = self.get_serializer(instance=conductor)
            return Response(serializer.data)

        return self.respond_error("Error de logueo")

    def get_dni(self, input_data):
        if 'dni' in input_data:
            return input_data['dni']
        else:
            return self.kwargs.get('dni', 0)



@api_view(['POST'])
def getPasajero(request):
    user = request.user
    if user:
        try:
            pasajero = Pasajero.objects.get(user=user)
        except Pasajero.DoesNotExist:
            pasajero = Pasajero.objects.create(dni=str(user.id), first_name=user.first_name,
                                                last_name=user.last_name, email=user.email, user=user)

        serializer = UserPasajeroSerializer(pasajero)
        return Response(serializer.data)

    return Response({RESPUESTA: USUARIO_INVALIDO})


@api_view(['POST'])
def getConductor(request):
    user = request.user
    if user:
        try:
            conductor = Conductor.objects.get(user=user)           
            serializer = UserConductorSerializer(conductor)
            return Response(serializer.data)
        except Conductor.DoesNotExist:
                return Response({RESPUESTA: DNI_INVALIDO})            

    return Response({RESPUESTA: USUARIO_INVALIDO})


@api_view(['POST'])
def asociarConductor(request):
    user = request.user
    input_data = request.data.copy()
    _dni = input_data['dni']

    if _dni == 0 or _dni is None:
        return Response({RESPUESTA: DNI_INVALIDO})

    try:
        conductor = Conductor.objects.get(dni=_dni)
        if conductor.user is None:
            conductor.user = user
            conductor.email = user.email
            conductor.save()
        elif conductor.user != user:
            return Response({RESPUESTA: USUARIO_INVALIDO})
    except Conductor.DoesNotExist:
        return Response({RESPUESTA: CONDUCTOR_NO_REGISTRADO})

    serializer = UserConductorSerializer(conductor)
    return Response(serializer.data)

from datetime import timedelta, datetime
import json

@login_required
def chat(request):

    time_threshold = datetime.now() - timedelta(hours=24)
    viajes_data = Viaje.objects.exclude(estado='F').filter(fecha_hora__gte=time_threshold).order_by('pasajero','-fecha_hora').distinct('pasajero')
    context = {}
    context['viajes']=viajes_data
    return render(request, 'chat.1.html', context)


def ver_mensajes(request, pk):

    time_threshold = datetime.now() - timedelta(hours=24)
    viajes_data = Viaje.objects.exclude(estado='F').filter(fecha_hora__gte=time_threshold).order_by('-fecha_hora').group_by('pasajero')
    context = {}
    context['viajes']=viajes_data
    context['pasajero']=Pasajero.objects.get(pk=pk)
    context['mensajes']= Mensaje.objects.filter(pasajero=pk).order_by('fecha')
    return render(request, 'chat/mensajes.html', context) 


def enviarMensaje(request):
    if request.is_ajax():
        pasajero = request.GET.get('id_pasajero',None)
        mensaje = request.GET.get('mensaje',None)
        print(mensaje)
        p = Pasajero.objects.get(pk = pasajero)
        nuevoMensaje = Mensaje.objects.create(conductor=None, pasajero=p, origen='B', titulo="", cuerpo_mensaje=mensaje, fecha=datetime.now())
        datos = {}
        data = json.dumps(datos)
        
        titulo = ""
        data_message = {
            MESSAGE_TYPE: MESSAGE_TYPE_CHAT,
            'data': mensaje,
            'fecha': str(nuevoMensaje.fecha),
            'origen': nuevoMensaje.origen
        }

        #Envía mensaje al pasajero
        if not p.firebase_token is None:
            try:
                result = push_service.notify_single_device(registration_id=p.firebase_token, message_body=titulo,
                                                        data_message=data_message)
            except FCMError as e:
                l.exception(u'%s; %s', e, str(e))

        try:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)("chat", {"type": "chat_message", "message":mark_safe(json.dumps(data_message))})
        except Exception as e:
            l.exception(u'%s; %s', e, str(e))
        
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404   


from channels.db import database_sync_to_async


@database_sync_to_async
def getMensajes(pk):
    from mensajes.serializers import ChatSerializer

    #time_threshold = datetime.now() - timedelta(hours=24)
    #viajes_data = Viaje.objects.exclude(estado='F').filter(fecha_hora__gte=time_threshold).order_by('-fecha_hora').distinct('pasajero')
    #context = {}
    #context['viajes']=viajes_data
    
    msjs = Mensaje.objects.filter(pasajero=pk).order_by('fecha')

    serializer = ChatSerializer(msjs, many=True)  

    return serializer.data


@database_sync_to_async
def grabarMensaje(pasajero, mensaje):
    p = Pasajero.objects.get(pk = pasajero)
    nuevoMensaje = Mensaje.objects.create(conductor=None, pasajero=p, origen='B', titulo="", cuerpo_mensaje=mensaje, fecha=datetime.now())
    datos = {}
    data = json.dumps(datos)
    
    titulo = ""
    data_message = {
        MESSAGE_TYPE: MESSAGE_TYPE_CHAT,
        'data': mensaje,
        'fecha': str(nuevoMensaje.fecha),
        'origen': nuevoMensaje.origen
    }

    #Envía mensaje al pasajero
    if not p.firebase_token is None:
        try:
            result = push_service.notify_single_device(registration_id=p.firebase_token, message_body=titulo,
                                                    data_message=data_message)
        except FCMError as e:
            l.exception(u'%s; %s', e, str(e))



@database_sync_to_async
def finalizarViaje(id):
    from viajes.serializers import PedidoSerializer
    
    try:    
        viaje = Viaje.objects.get(pk=id)
        viaje.estado = 'F'
        viaje.fecha_hora_fin = datetime.now()
        viaje.save()

        serializer = PedidoSerializer(viaje)
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)("pedidos", {"type": "chat_message", "message":serializer.data })
    except Exception as e:
        l.exception(u'%s; %s', e, str(e))

    return serializer.data
