from django.conf import settings
from django.db import models

class Userthumb(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    social_thumb = models.URLField(null=True, blank=True)
