import hashlib

from django.shortcuts import redirect
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from users.models import Userthumb

def auto_logout(*args, **kwargs):
    """Do not compare current user with new one"""
    return {'user': None}


def save_avatar(strategy, details, user = None, *args, **kwargs):
    """Get user avatar from social provider."""
    if user:
        backend_name = kwargs['backend'].__class__.__name__.lower()
        response = kwargs.get('response', {})
        social_thumb = None
        if 'facebook' in backend_name:
            if 'id' in response:
                social_thumb = 'http://graph.facebook.com/{0}/picture?type=normal'.format(response['id'])
        elif 'twitter' in backend_name and response.get('profile_image_url'):
            social_thumb = response['profile_image_url']
        elif 'googleoauth2' in backend_name and response.get('image', {}).get('url'):
            social_thumb = response['image']['url'].split('?')[0]
        else:
            social_thumb = 'http://www.gravatar.com/avatar/'
            social_thumb += hashlib.md5(user.email.lower().encode('utf8')).hexdigest()
            social_thumb += '?size=100'
        if social_thumb:
            try:
                userthumb = Userthumb.objects.get(user=user)
            except Userthumb.DoesNotExist:
                userthumb = Userthumb.objects.create(user=user, social_thumb=social_thumb)

            if userthumb.social_thumb != social_thumb:
                userthumb.social_thumb = social_thumb
            userthumb.save()


def create_token(user=None, *args, **kwargs):
    if user:
        try:
            token = Token.objects.get(user=user)
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)
        token.save()


def check_for_email(backend, uid, user = None, *args, **kwargs):
    if not kwargs['details'].get('email'):
        return Response({'error': "Email wasn't provided by facebook"}, status=400)


def redirect_if_no_refresh_token(backend, response, social, *args, **kwargs):
    if backend.name == 'google-oauth2' and social and response.get('refresh_token') is None and social.extra_data.get('refresh_token') is None:
        return redirect('/login/google-oauth2?approval_prompt=force')
