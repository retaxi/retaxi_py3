from django.contrib import admin
from django.contrib.gis import admin
from .models import Zona

# Register your models here.
class ZonaAdmin(admin.OSMGeoAdmin):
    list_display = ['nombre', 'hora_desde', 'hora_hasta']
    search_fields = ['nombre', 'hora_desde', 'hora_hasta']
    default_lon = -6547794.49
    default_lat = -3183316.26
    default_zoom = 13

    class Meta:
        model = Zona



admin.site.register(Zona, ZonaAdmin)