from .models import Zona
from rest_framework import serializers


class ZonaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Zona
        fields = ('id',
                  'nombre', 'poli', 'base')
