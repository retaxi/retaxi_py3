from django.db import models
from django.contrib.gis.db import models
from datetime import datetime


class Zona(models.Model):

    nombre = models.CharField(verbose_name='nombre', max_length=50, )
    number = models.CharField(
        verbose_name='numero', max_length=2, null=False, blank=False, default="?")
    poli = models.PolygonField()
    base = models.PointField(null=True, blank=True,
                             default='POINT(0 0)', srid=4326)
    hora_desde = models.TimeField(
        null=False, blank=False, default='00:00:00.000')
    hora_hasta = models.TimeField(
        null=False, blank=False, default='23:59:59.999')
    activated = models.BooleanField(
        null=False, blank=False, default=True, verbose_name='Activa')

    def __str__(self):
        return '%s' % (self.nombre)

    def obtenerConductores2(self):
        return self.conductores_zonas.filter(estado='a', abonado='s').order_by('hora_asignacion')
