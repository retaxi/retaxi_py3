from django.shortcuts import render
from .models import Zona
from .serializers import ZonaSerializer
from conductores.models import Conductor
from django.views.generic.list import ListView
from django.http import HttpResponse, Http404
from django.utils.safestring import mark_safe
import json


def mapaZonas(request):
    zonas_data = Zona.objects.filter(activated=True)

    serializer = ZonaSerializer(zonas_data, many=True)

    context = {
        'zonas_json': mark_safe(json.dumps(serializer.data))
    }

    return render(request, 'zonasMapa.html', context)
