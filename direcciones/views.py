from rest_framework.response import Response
import googlemaps
from django.conf import settings
from rest_framework.decorators import api_view
from uuid import uuid4 as autocomplete_session_token

RADIO = settings.GMAPS_RADIO
gmaps = googlemaps.Client(key=settings.GMAPS_API_KEY)


@api_view(['GET'])
def ruta(request, origen, destino):
    directions_result = gmaps.directions(origen, destino)
    return Response({"routes": directions_result})


@api_view(['GET'])
def lugares(request, input, location):
    component_filter = {'country': 'AR'}
    resistencia = {
        "lat": -27.451014,
        "lng": -58.986587
    }
    corrientes = {
        "lat": -27.479528,
        "lng": -58.812990
    }
    result = gmaps.places_autocomplete(input_text=input, session_token=autocomplete_session_token, offset=None,
                                       location=corrientes, radius=RADIO, language='es', types=None, components=component_filter, strict_bounds=True)
    #result = gmaps.find_place(input=input, input_type=location, fields=None, location_bias=None, language=None)
    return Response({"predictions": result})


@api_view(['GET'])
def detalle_lugar(request, place_id):
    return Response(gmaps.place(place_id=place_id, session_token=autocomplete_session_token, fields=None, language=None))


@api_view(['GET'])
def getinfodire(request, direccion):
    return Response(gmaps.geocode(direccion))


def distancia(origen, destino):
    return gmaps.distance_matrix(origen, destino, mode='driving', language='es', units='metric')


def getLatLng(address):
    return gmaps.geocode(address)[0]['geometry']['location']
