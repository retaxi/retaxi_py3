from django.contrib import admin
from .models import Conductor, Movil


# Register your models here.
class MovilAdmin(admin.ModelAdmin):
    # inlines = (MovilInline,)
    list_display = ['marca', 'model', 'patente', ]
    search_fields = ['conductor__first_name', 'conductor__last_name']
    list_filter = ['conductor__first_name', 'conductor__last_name']

    class Meta:
        model = Movil


def make_inactivo(self, request, queryset):
    rows_updated = queryset.update(estado='i')
    if rows_updated == 1:
        message_bit = "1 chofer fue"
    else:
        message_bit = "%s choferes fueron" % rows_updated
    self.message_user(request, "%s marcado/s como inactivo/s" % message_bit)


make_inactivo.short_description = "Marcar como inactivo"


def make_noabono(self, request, queryset):
    rows_updated = queryset.update(abonado='n')
    if rows_updated == 1:
        message_bit = "1 chofer fue"
    else:
        message_bit = "%s choferes fueron" % rows_updated
    self.message_user(request, "%s marcado/s como no abonado/s" % message_bit)


make_noabono.short_description = "Marcar como no abonado"


class ConductorAdmin(admin.ModelAdmin):
    # inlines = (MovilInline,)
    list_display = ['first_name', 'last_name', 'registro', 'dni', 'zona',
                    'fecha_vencimiento_abono', 'hora_asignacion', 'telephone', 'estado', 'abonado']
    list_filter = ['zona']
    search_fields = ['first_name', 'last_name', 'dni', 'registro', ]
    filter_horizontal = ['movil', ]
    actions = [make_inactivo]
    exclude = ['ultima_ubicacion', 'reputacion', 'fecha_ultima_ubicacion', 'fecha_vencimiento_habilitacion',
               'abonado', 'viajes_realizados', 'firebase_token', 'hora_asignacion', 'zona']

    class Meta:
        model = Conductor

# class ConZAdmin(admin.ModelAdmin):
#    list_display = ['zona', 'conductor', 'hora_asignacion',]
#
#    class Meta:
#        model = Conductor
#


admin.site.register(Movil, MovilAdmin)
admin.site.register(Conductor, ConductorAdmin)
# admin.site.register(ConductorEnZona, ConZAdmin)
