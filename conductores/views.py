from datetime import datetime

from django.contrib.gis.geos import Point
from rest_framework import generics
from rest_framework.decorators import api_view, detail_route
from rest_framework.response import Response
from rest_framework import viewsets, status
from django.views.generic.list import ListView
from django.utils.safestring import mark_safe
import json
from django.shortcuts import render
from .serializers import ConductorSerializer, MovilSerializer
from .models import Conductor, Movil
from time import time
import logging

APP_NAME = 'conductor'


class ConductorViewSet(viewsets.ModelViewSet):
    queryset = Conductor.objects.all()
    serializer_class = ConductorSerializer

    # @detail_route(methods=['post'])
    # def set_movil(self, request, pk=None):
    #     # get conductor object
    #     my_conductor = Conductor.objects.get(conductor=self.get_object())
    #     serializer = MovilSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         my_conductor.movil.add(serializer)
    #         my_conductor.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ConductorDetail(generics.DestroyAPIView,
                      generics.UpdateAPIView,
                      generics.RetrieveAPIView):
    queryset = Conductor.objects.all()
    serializer_class = ConductorSerializer


class ConductorList(generics.ListAPIView):
    queryset = Conductor.objects.all()
    serializer_class = ConductorSerializer


class MovilList(generics.ListAPIView):
    queryset = Movil.objects.all()
    serializer_class = MovilSerializer


class MovilDetail(generics.RetrieveAPIView):
    queryset = Movil.objects.all()
    serializer_class = MovilSerializer


#from rest_framework_gis_distance.filters import OrderedDistanceToPointFilter


# class LocationList(generics.ListAPIView):
#    queryset = Conductor.objects.all()
#    serializer_class = ConductorSerializer
#    filter_backends = (OrderedDistanceToPointFilter,)
#    distance_filter_field = 'ultima_ubicacion'
#    distance_filter_convert_meters = True
#    distance_filter_add_distance = True


@api_view(['POST'])
def actualizar_ubicacion(request):
    try:
        comienzo = time()
        conductor = Conductor.objects.get(user=request.user)
        data = request.data.copy()
        coordenadas = data['posicion'].split(',')
        conductor.ultima_ubicacion = Point(
            float(coordenadas[0]), float(coordenadas[1]), srid=4326)
        conductor.fecha_ultima_ubicacion = datetime.now()
        conductor.save()

    except Conductor.DoesNotExist:
        return Response({'respuesta': 'ERROR'})
    return Response({'respuesta': 'OK'})


@api_view(['POST'])
def set_movil(request):
    try:
        conductor = Conductor.objects.get(user=request.user)
        data = request.data.copy()

        try:
            movil = Movil.objects.get(patente=data['patente'])
        except Movil.DoesNotExist:
            movil = Movil.objects.create(
                marca=data['marca'], model=data['model'], patente=data['patente'])
            movil.save()

        conductor.movil.add(movil)
        conductor.save()
    except Conductor.DoesNotExist:
        return Response({'respuesta': 'ERROR'})
    return Response({'respuesta': 'OK'})


@api_view(['POST'])
def remove_movil(request):
    try:
        conductor = Conductor.objects.get(user=request.user)
        data = request.data.copy()

        try:
            movil = Movil.objects.get(patente=data['patente'])
        except Movil.DoesNotExist:
            return Response({'respuesta': 'ERROR'})

        conductor.movil.remove(movil)
        conductor.save()
    except Conductor.DoesNotExist:
        return Response({'respuesta': 'ERROR'})
    return Response({'respuesta': 'OK'})


@api_view(['POST'])
def conductor_msg_token(request):
    try:
        conductor = Conductor.objects.get(user=request.user)
        data = request.data.copy()
        token = data['token']
        conductor.firebase_token = token
        conductor.save()

    except Conductor.DoesNotExist:
        return Response({'respuesta': 'ERROR'})
    return Response({'respuesta': 'OK'})


def mapaMoviles(request):
    moviles_data = Conductor.objects.all()

    serializer = ConductorSerializer(moviles_data, many=True)

    context = {
        'moviles_json': mark_safe(json.dumps(serializer.data))
    }

    return render(request, 'mapaMoviles.html', context)
