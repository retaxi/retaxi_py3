from datetime import datetime
from django.utils import timezone
from django.conf import settings
from django.contrib.gis.db import models
from zona.models import Zona


class Movil(models.Model):

    marca = models.CharField(verbose_name='marca', max_length=50)
    model = models.CharField(verbose_name='modelo', max_length=20)
    patente = models.CharField(
        verbose_name='patente', unique=True, max_length=9)
    color = models.CharField(verbose_name='color', max_length=20)

    def __str__(self):
        return '%s %s %s' % (self.marca, self.model, self.patente)

    class Meta:
        verbose_name = 'movil'
        verbose_name_plural = 'moviles'


class Conductor(models.Model):
    dni = models.CharField(verbose_name='dni', primary_key=True,
                           unique=True, max_length=8, blank=False, null=False)
    first_name = models.CharField(
        verbose_name='nombre', max_length=50, blank=False, null=False)
    last_name = models.CharField(
        verbose_name='apellido', max_length=50, blank=False, null=False)
    registro = models.CharField(
        verbose_name='registro', max_length=20, blank=True, null=False)
    empresa = models.CharField(
        verbose_name='empresa', max_length=50, blank=True, null=True)
    birthday = models.DateField(
        verbose_name='fecha de nacimiento', blank=True, null=True)

    SEXO = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
    )

    STATUS = (
        ('a', 'Activo'),
        ('i', 'Inactivo'),
    )

    SINO = (
        ('s', 'Si'),
        ('n', 'No'),
    )

    telephone = models.CharField(
        verbose_name='telefono', max_length=20, blank=False, null=False)
    email = models.CharField(verbose_name='email', max_length=100)
    sexo = models.CharField(max_length=1, choices=SEXO,
                            blank=False, null=False)
    movil = models.ManyToManyField(Movil, blank=True)
    estado = models.CharField(max_length=1, choices=STATUS, default='i')
    abonado = models.CharField(max_length=1, choices=SINO, default='n')
    ultima_ubicacion = models.PointField(
        null=True, blank=True, default='POINT(0 0)', srid=4326)
    fecha_ultima_ubicacion = models.DateTimeField(default='1900-01-01')
    reputacion = models.FloatField(blank=True, null=True, default=1)
    viajes_realizados = models.IntegerField(blank=True, null=True, default=0)
    fecha_vencimiento_habilitacion = models.DateTimeField(
        blank=True, null=True)
    fecha_vencimiento_abono = models.DateTimeField(blank=True, null=True)
    zona = models.ForeignKey(Zona, blank=True, null=True,
                             on_delete=models.DO_NOTHING, related_name='conductores_zonas')
    hora_asignacion = models.DateTimeField(default=timezone.now)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True, editable=False)

    firebase_token = models.TextField(
        verbose_name='firebase_token', blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def esAbonado(self):
        if self.fecha_vencimiento_abono is None:
            return "true"
        else:
            if self.fecha_vencimiento_abono >= timezone.now():
                return "true"
            else:
                return "false"

    class Meta:
        verbose_name = 'conductor'
        verbose_name_plural = 'conductores'

# class ConductorEnZona(models.Model):

#    conductor = models.OneToOneField(Conductor,primary_key=True, unique=True, on_delete=models.DO_NOTHING)
#    zona = models.ForeignKey(Zona, on_delete=models.DO_NOTHING, related_name='conductores_cabezona')
#    hora_asignacion = models.DateTimeField(default=timezone.now)

 #   def __str__(self):
 #       return super(ConductorEnZona, self).__str__()
