from .models import Conductor, Movil
from rest_framework import serializers 
from rest_framework.authtoken.models import Token
from users.models import Userthumb

#from users.serializers import UserThumbSerializer
# Serializers define the API representation.
class MovilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movil
        fields = ('marca',
        	'model', 
        	'patente',
            'color'
            )

class UserConductorSerializer(serializers.ModelSerializer):    
    social_thumb = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()

    def get_social_thumb(self, obj):
        try:
            userthumb = Userthumb.objects.get(user=obj.user)
        except Userthumb.DoesNotExist:
            return ''

        return userthumb.social_thumb

    def get_token(self, obj):
        if obj.user:
            token, created = Token.objects.get_or_create(user=obj.user)
            return token.key
        return ""

    #moviles = MovilSerializer(many=True, read_only=True, source='movil_set')

    class Meta:
        model = Conductor
        
        fields = ('dni',
            'first_name', 
            'last_name', 
            'registro',
            'empresa', 
            'email', 
            'telephone', 
            'movil',
            'ultima_ubicacion',
            'reputacion',
            'viajes_realizados',
            'fecha_ultima_ubicacion', 
            'social_thumb',
            'token' )
        depth = 2   

class ConductorSerializer(serializers.ModelSerializer):
    #moviles = MovilSerializer(many=True, read_only=True, source='movil_set')
    esAbonado = serializers.SerializerMethodField()

    def get_esAbonado(self, obj):
        return obj.esAbonado()
        
    class Meta:
        model = Conductor
        fields = ('dni',
        	'first_name', 
        	'last_name', 
        	'registro',
            'esAbonado',
        	'empresa', 
        	'email', 
        	'telephone', 
        	'movil',
            'ultima_ubicacion',
            'reputacion',
            'viajes_realizados',
            'fecha_ultima_ubicacion',
            'firebase_token',
            'fecha_vencimiento_habilitacion',
            'fecha_vencimiento_abono', )
        depth = 2   