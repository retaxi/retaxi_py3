from .models import Mensaje
from rest_framework import serializers
from pasajeros.serializers import UserPasajeroSerializer
from conductores.serializers import UserConductorSerializer


class MensajeSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    pasajero = UserPasajeroSerializer(many=False, read_only=True)
    conductor = UserConductorSerializer(many=False, read_only=True)

    class Meta:
        model = Mensaje

        fields = (
        	'conductor',
            'pasajero',
            'origen',
            'titulo',
            'cuerpo_mensaje',
            'fecha' )


class ChatSerializer(serializers.ModelSerializer):
    hora = serializers.SerializerMethodField()
    fecha = serializers.SerializerMethodField()

    def get_hora(self, obj):
        return obj.fecha.strftime('%H:%M')

    def get_fecha(self, obj):
        return obj.fecha.strftime('%d/%m/%Y')

    class Meta:
        model = Mensaje

        fields = (
            'origen',
            'titulo',
            'cuerpo_mensaje',
            'fecha',
            'hora' )