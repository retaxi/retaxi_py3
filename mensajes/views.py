# -*- coding: utf-8 -*-
import logging
from pyfcm import FCMNotification
from pyfcm.errors import FCMError
from rest_framework import authentication, generics, permissions, status
from rest_framework.response import Response
from datetime import datetime
from conductores.models import Conductor
from mensajes import *
from mensajes.models import Mensaje
from mensajes.serializers import MensajeSerializer
from pasajeros.models import Pasajero
import json
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.utils.safestring import mark_safe

l = logging.getLogger(__name__)

push_service = FCMNotification(api_key=FIREBASE_API_KEY)

class BaseView(generics.GenericAPIView):
    serializer_class = None
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (authentication.TokenAuthentication,)

    def resultado_solicitud(self, result, objects):
        """

        :param result: resultado del servico FCM
        :type objects: Pasajero o Conductor a cambiar firebase_token
        """
        if result['failure'] == 0 and result['canonical_ids'] == 0:
            return Response({MESSAGE_RESPUESTA: SOLICITUD_ENVIADA})
        else:
            i = 0
            error = ''
            for r in result['results']:
                if 'message_id' in r:
                    if 'registration_id' in r:
                        new_token = r['registration_id']
                        if type(objects) == 'list':
                            o = objects[i]
                        else:
                            o = objects
                        o.firebase_token = new_token
                        o.save()
                else:
                    if 'error' in r:
                        error += '/n' + r['error']
                i += 1

            if result['success'] == 0:
                # todos fallidos
                return self.respond_error(error)
            elif len(error) > 0:
                self.log_exception(error)

            if result['success'] > 0:
                # OK
                return Response({MESSAGE_RESPUESTA: SOLICITUD_ENVIADA})

    def respond_error(self, error):
        if isinstance(error, Exception):
            self.log_exception(error)
        else:
            l.error(error)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def log_exception(self, error):
        if getattr(error, 'response', None) is not None:
            if type(error) == 'str':
                err_msg = error
            else:
                err_msg = error.args[0]
            try:
                err_data = error.response.json()
            except (ValueError, AttributeError):
                l.error(u'%s; %s', error, err_msg)
            else:
                l.error(u'%s; %s; %s', error, err_msg, err_data)
        else:
            l.exception(u'%s; %s', error, str(error))


class EnviarMensajeView(BaseView):
    """
       Vista para que un pasajero pueda solicitar remises

       **Input**:

           {
               "origen": {"C"|"P"}
               "destino": {pasajero_id | conductor_id}
               "titulo": "titulo o parte del mensaje",
               "mensaje": "Mensaje o chat max 400 car".
               "tipo_mensaje": {"C"|"R"}
           }

       **Output**:

       """

    def post(self, request, *args, **kwargs):
        """ Enviar Mensaje """

        input_data = request.data.copy()
        origen = input_data['origen']
        destino = input_data['destino']
        
        if CHAT_BASE:
            if origen == 'C':
                try:
                    conductor = Conductor.objects.get(user=request.user)
                    pasajero=None
                except Conductor.DoesNotExist:
                    return Response({MESSAGE_RESPUESTA: CONDUCTOR_INVALID})
            elif origen == 'P':
                try:
                    pasajero = Pasajero.objects.get(user=request.user)
                    conductor=None
                except Pasajero.DoesNotExist:
                    return Response({MESSAGE_RESPUESTA: PASAJERO_INVALID})
        else:
            if origen == 'C':
                try:
                    conductor = Conductor.objects.get(user=request.user)
                    pasajero = Pasajero.objects.get(user=destino)
                    receptor = pasajero
                except Conductor.DoesNotExist:
                    return Response({MESSAGE_RESPUESTA: CONDUCTOR_INVALID})
                except Pasajero.DoesNotExist:
                    return Response({MESSAGE_RESPUESTA: PASAJERO_INVALID})
            elif origen == 'P':
                try:
                    pasajero = Pasajero.objects.get(user=request.user)
                    conductor = Conductor.objects.get(dni=destino)
                    receptor = conductor
                except Pasajero.DoesNotExist:
                    return Response({MESSAGE_RESPUESTA: PASAJERO_INVALID})
                except Conductor.DoesNotExist:
                    return Response({MESSAGE_RESPUESTA: CONDUCTOR_INVALID})

            fire_token = receptor.firebase_token
        #endif

        titulo = input_data['titulo']
        mensaje_data = input_data['mensaje']
        tipo = 'C'
        if 'tipo_mensaje' in input_data:
            tipo = input_data['tipo_mensaje']

        mensaje = Mensaje(conductor=conductor, pasajero=pasajero, origen=origen, titulo=titulo,
                          cuerpo_mensaje=mensaje_data,
                          fecha=datetime.now(), tipo=tipo)
        mensaje.save()

        data_message = {
            MESSAGE_TYPE: MESSAGE_TYPE_CHAT,
            'data': mensaje_data,
            'fecha':str(mensaje.fecha),
            'tipo': tipo, 
            'origen':origen
        }

        """ Mando el mensaje al Socket del Chat con pasajero """
        mensaje_json = mark_safe(json.dumps(data_message))

        try:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)("chat", {"type": "chat.message", "room_id": str(pasajero.pk), "username":"", "message":mensaje_json })
        except Exception as e:
            return self.respond_error(e)

        if not CHAT_BASE:    
            try:
                result = push_service.notify_single_device(registration_id=fire_token, message_body=titulo,
                                                        data_message=data_message)
            except FCMError as e:
                return self.respond_error(e)

            return self.resultado_solicitud(result, receptor)

        return Response({MESSAGE_RESPUESTA: SOLICITUD_ENVIADA})