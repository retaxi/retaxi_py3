from datetime import datetime
from django.db import models
from conductores.models import Conductor
from pasajeros.models import Pasajero
from django.conf import settings


class Mensaje(models.Model):
    ORIGEN = (('C', 'Conductor'), ('P', 'Pasajero'), ('B', 'Base'))
    TIPO = (('C', 'Chat'), ('R', 'Reclamo'))

    conductor = models.ForeignKey(Conductor,blank=True, null=True, on_delete=models.CASCADE)
    pasajero = models.ForeignKey(Pasajero, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, null=True, blank=True, editable=False, related_name='receptor')
    origen = models.CharField(max_length=1, choices=ORIGEN, blank=False, null=False)
    titulo = models.CharField(verbose_name='titulo', max_length=50, blank=True, null=False)
    cuerpo_mensaje = models.CharField(verbose_name='cuerpo_mensaje', max_length=400, blank=False, null=False)
    fecha = models.DateTimeField(verbose_name='fecha', blank=True)
    tipo = models.CharField(max_length=1, choices=TIPO, blank=False, null=False, default="C")

    def __str__(self):
        return '%s %s %s' % (self.origen, self.titulo, self.cuerpo_mensaje)
