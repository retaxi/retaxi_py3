$(document).ready(function () {
    $('.js-example-basic-single').select2();

    $('.tablas').DataTable({
        select: true,
        select: {
            style: 'single'
        },
        order: [[6, 'desc'], [3, 'desc']],
        keys: true,
        "iDisplayLength": 25,
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            select: {
                rows: {
                    _: "%d filas seleccionadas",
                    1: "1 fila seleccionada"
                }
            }
        },
    });
    var table = $('#myTable').DataTable();
    table.rows(':eq(0)').select();
});

$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        $(this).siblings('select').select2('open');
    }
});

var activeTab = localStorage.getItem('activeTab');
if (activeTab) {
    $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
}

$('.wrapper').on('click', '.cambiarEstado', function (event) {
    var flag = true;
    var x = $(this).attr("id");
    var conductor = $('#selectviaje' + x).val();
    if (conductor != null){
        $('#accion' + x).html('<div class="loader"></div>');
        $('#aencurso' + x).html('<div class="loader"></div>');
        $('#acompletado' + x).html('<div class="loader"></div>');
    } 
    var y = document.getElementById("snackbar");
    
    $.ajax({
        data: { 'id_viaje': x, 'id_conductor': conductor, 'flag': flag },
        url: "/estado/",
        type: 'get',
        cache: false,
        success: function (data) {
            //console.log("cambio de estado. OK");
        },
        error: function (data) {
            console.log("Error");
            $('#snackbar').html('Error no hay conductores en esa zona');
            y.className = "show";
            setTimeout(function () { y.className = y.className.replace("show", ""); }, 3000);
        }
    });
    return false;
});

$('.wrapper').on('click', '.cancelar', function (event) {
    var x = $(this).attr("id");
    $.ajax({
        data: { 'id_viaje': x },
        url: "/cancelar/",
        type: 'get',
        cache: false,
        success: function (data) {
            //console.log("Cancelado: "+data);                                       
        },
        fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
});

$('.wrapper').on('click', '.editar', function (event) {
    var x = $(this).attr("id");
    $('#accion' + x).html('<a href="#" class="cambiarEstado" id="' + x + '"><i class="os-icon os-icon-ui-49"></i></a><a href="#" class="atras" id="' + x + '"><i class="os-icon os-icon-common-07"></i></a>');
    $.ajax({
        data: { 'id_viaje': x },
        url: "/editarPedido/",
        type: 'get',
        cache: false,
        success: function (data) {
            salida = '<select class="form-control js-example-basic-single zona' + data.zona + '" id="selectviaje' + x + '">'

            for (var item in data.conductores) {
                salida += '<option value="' + data.conductores[item].dni + '" class="porzona"' + data.conductores[item].dni + '>' + data.conductores[item].registro + '</option>'
            }

            salida += '</select>'
            $('#conductor' + x).html(salida);
            $('#selectviaje' + x).select2();
        },
        fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
});

//para editar la zona, si es que cayo en una incorrect
$('.wrapper').on('click', '.editarZona', function (event) {
    var x = $(this).attr("id");
    $('#accion' + x).html('<a href="#" class="cambiarZona" id="' + x + '"><i class="os-icon os-icon-ui-49"></i></a><a href="#" class="atras" id="' + x + '"><i class="os-icon os-icon-common-07"></i></a>');
    $.ajax({
        data: { 'id_viaje': x },
        url: "/editarZona/",
        type: 'get',
        cache: false,
        success: function (data) {
            salida = '<select class="form-control js-example-basic-single" id="selectzona">'
            for (var item in data.zonas) {
                salida += '<option value="' + data.zonas[item].id + '" class="cambiarZona lazona"' + data.zonas[item].id + '>' + data.zonas[item].nombre + '</option>'
            }
            salida += '<option value="0" class="cambiarZona lazona">Sin Zona</option>'
            salida += '</select>'
            $('#editarzona' + x).html(salida);
            $('#selectzona').select2();
            $("#selectviaje" + x).prop("disabled", true);
        },
        fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
});

$('.wrapper').on('click', '.cambiarZona', function (event) {
    var x = $(this).attr("id");
    var zona = $('#selectzona').val();
    $.ajax({
        data: { 'id_viaje': x, 'id_zona': zona },
        url: "/cambiarZona/",
        type: 'get',
        cache: false,
        success: function (data) {
            //El cambio viene por el socket
        },
        fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
});

$('.wrapper').on('click', '.atras', function (event) {
    var x = $(this).attr("id");
    location.reload();
});

$('.wrapper2').on('click', '.asignarZona', function (event) {
    var zona = $('#select1').val();
    var conductor = $('#select2').val();
    $('.chat-btn').html('<div class="loader"></div>');
    var y = document.getElementById("snackbar");

    $.ajax({
        data: { 'id_zona': zona, 'id_conductor': conductor },
        url: "/asignarZona/",
        type: 'get',
        cache: false,
        success: function (data) {
            $('.chat-btn').html('<a class="btn btn-primary asignarZona" href="#">Confirmar</a>');
            //console.log("rebiennae");                                         
        },
        fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
});

$('#select2').on('select2:select', function (e) {
    $('.asignarZona').click();
});

$('.wrapper2').on('click', '.quitarZona', function (event) {
    var x = $(this).attr("id");
    var y = document.getElementById("snackbar");

    $.ajax({
        data: { 'id_conductor': x },
        url: "/quitarZona/",
        type: 'get',
        cache: false,
        success: function (data) {
            //console.log("rebiennae");                                        
        }, fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
});