hotkeys('*', function (event, handler) {
    // Asignación de móviles a Zonas
    if ('F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11'.indexOf(event.key) === -1)
        // Cambia Tabs 
        if ('1,2,3,4'.indexOf(event.key) === -1)
            // filtros por estado
            if ('r,q,w,e,t,R,Q,W,E,T'.indexOf(event.key) === -1)
                // Acciones Tabla
                if ('ArrowUp,ArrowDown,a,s,d,f,g,A,S,D,F,G'.indexOf(event.key) === -1)
                    return;

    event.preventDefault();

    switch (event.key) {
        case "1":
            $('#nav-home-tab').click();
            localStorage.setItem('activeTab', $('#nav-home-tab').attr('href'));
            location.reload();
            break;
        case "2":
            $('#nav-profile-tab').click();
            localStorage.setItem('activeTab', $('#nav-profile-tab').attr('href'));
            location.reload();
            break;
        case "3":
            $('#nav-contact-tab').click();
            localStorage.setItem('activeTab', $('#nav-contact-tab').attr('href'));
            location.reload();
            break;
        case "4":
            $('#nav-abordo-tab').click();
            localStorage.setItem('activeTab', $('#nav-abordo-tab').attr('href'));
            location.reload();
            break;

        case "F1":
            $('#select2').select2('open');
            $('#select1').val('3').trigger('change');
            break;
        case "F2":
            $('#select2').select2('open');
            $('#select1').val('4').trigger('change');
            break;
        case "F3":
            $('#select2').select2('open');
            $('#select1').val('5').trigger('change');
            break;
        case "F4":
            $('#select2').select2('open');
            $('#select1').val('6').trigger('change');
            break;
        case "F5":
            $('#select2').select2('open');
            $('#select1').val('7').trigger('change');
            break;
        case "F6":
            $('#select2').select2('open');
            $('#select1').val('8').trigger('change');
            break;
        case "F7":
            $('#select2').select2('open');
            $('#select1').val('9').trigger('change');
            break;
        case "F8":
            $('#select2').select2('open');
            $('#select1').val('10').trigger('change');
            break;
        case "F9":
            $('#select2').select2('open');
            $('#select1').val('11').trigger('change');
        case "F10":
            $('#select2').select2('open');
            $('#select1').val('12').trigger('change');
        case "F11":
            $('#select2').select2('open');
            $('#select1').val('2').trigger('change');
            break;
        case "q":
            $('#myTable_filter input').focus();
            $("#myTable_filter label input").val("Pendiente");
            break;
        case "w":
            $('#myTable_filter input').focus();
            $("#myTable_filter label input").val("En curso");
            break;
        case "e":
            $("#myTable_filter label input").val("Cancelado");
            $('#myTable_filter input').focus();
            break;
        case "r":
            $("#myTable_filter label input").val("Completado");
            $('#myTable_filter input').focus();
            break;
        case "t":
            $("#myTable_filter label input").val("a bordo");
            $('#myTable_filter input').focus();
            break;
        case "ArrowUp":
            var table = $('#myTable').DataTable();
            var fila = table.row({ selected: true }).node();
            var idx = table.rows({ order: 'current' }).nodes().indexOf(fila) - 1;
            table.rows(':eq(' + idx + ')').select();
            break;
        case "ArrowDown":
            var table = $('#myTable').DataTable();
            var fila = table.row({ selected: true }).node();
            var idx = table.rows({ order: 'current' }).nodes().indexOf(fila) + 1;
            table.rows(':eq(' + idx + ')').select();
            break;
        case "a":
            var table = $('#myTable').DataTable();
            var fila = table.row({ selected: true }).node();
            $(fila).find('select').focus();
            break;
        case "s":
            var table = $('#myTable').DataTable();
            var fila = table.row({ selected: true }).node();
            $(fila).find('.cambiarEstado').click();
            break;
        case "d":
            var table = $('#myTable').DataTable();
            var fila = table.row({ selected: true }).node();
            $(fila).find('.editar').click();
            break;
        case "f":
            var table = $('#myTable').DataTable();
            var fila = table.row({ selected: true }).node();
            $(fila).find('.cancelar').click();
            break;
        case "g":

            break;
    }
});