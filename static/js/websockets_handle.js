var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
var ws_path = ws_scheme + '://' + window.location.host + "/ws/pedidos/";
var chatSocket = new ReconnectingWebSocket(ws_path);

chatSocket.onmessage = function (e) {
    var data = JSON.parse(e.data);
    var message = data['message'];

    if (typeof message === "string") {
        message = JSON.parse(message);
    }
    //document.querySelector('#chat-log').value += (message + '\n');
    switch (message.tipo) {
        case 'add_zona':
            asignar_zona(message);
            break;
        case 'rm_zona':
            quitar_zona(message);
            break;
        case 'chng_zona':
            var z = message.zona;
            $('#itemes' + z).find('.item').remove();
            $('#itemes' + z).find('.itemRojo').remove();
            message.conductores_zona.forEach(c => {
                if (c.esAbonado == "true") {
                    $('#itemes' + z).append('<div class="item" id="' + c.dni + '"><div class="inactivo quitarZona" id="' + c.dni + '" style="cursor: pointer;">' + c.registro + '</div></div>');
                } else {
                    $('#itemes' + z).append('<div class="itemRojo" id="' + c.dni + '"><div class="inactivo quitarZona" id="' + c.dni + '" style="cursor: pointer;">' + c.registro + '</div></div>');
                }
            });
            break;
        case 'edit_zona':
            var x = message.viaje;
            salida = '<select class="form-control js-example-basic-single zona' + message.zona + '" id="selectviaje' + x + '">'
            for (var item in message.conductores) {
                salida += '<option value="' + message.conductores[item].dni + '" class="porzona"' + message.conductores[item].dni + '>' + message.conductores[item].registro + '</option>'
            }
            salida += '</select>'

            $('#conductor' + x).html(salida);
            $('#selectviaje' + x).select2();
            $('#editarzona' + x).html(message.nombrezona);
            $("#selectviaje" + x).prop("enable", true);
            $('#accion' + x).html('<a href="#" class="cambiarEstado" id="' + x + '"><i class="os-icon os-icon-arrow-down4"></i></a><a href="#" class="editarZona" id="' + x + '"><i class="os-icon os-icon-ui-49"></i></a><a href="#" class="cancelar" id="' + x + '"><i class="os-icon os-icon-cancel-circle"></i></a>');
            break;

        default:
            if (message.estado == 'P') {
                cargar_datos(message);
            } else {
                cambiar_estado(message);
            }
            break;
    }
};

chatSocket.onclose = function (e) {
    console.error('Chat socket closed unexpectedly');
};

cargar_datos = function (datos) {
    var id = datos.id;
    var urlCompleta = "/detallePedido/" + id + "/";

    var select_conductores = ''
    var zona = ''
    var zona_name = 'Sin zona'
    if (datos.zona != null && datos.zona != '') {
        zona = datos.zona.id
        zona_name = datos.zona.nombre
        if (datos.estado != 'P') {
            select_conductores = '<select class="form-control js-example-basic-single zona' + zona + '" id="selectviaje' + datos.id + '">'
            for (var item in datos.conductores_zona) {
                select_conductores += '<option value="' + datos.conductores_zona[item].dni + '">' + datos.conductores_zona[item].registro + '</option>'
            }
            select_conductores += '</select>'
        }
    } else {
        if (datos.estado == 'P') {
            select_conductores = '<select class="form-control js-example-basic-single zona' + zona + '" id="selectviaje' + datos.id + '">'
            for (var item in datos.conductores_all) {
                select_conductores += '<option value="' + datos.conductores_all[item].dni + '" class="porzona"' + datos.conductores_all[item].dni + '>' + datos.conductores_all[item].registro + '</option>'
            }
            select_conductores += '</select>'
        } else {
            select_conductores = datos.movil
        }
    }

    var row = $('<tr id="viaje"' + id + '>')
        .append('<td>' + datos.nombre_pasajero + '</td>')
        .append('<td><a href="' + urlCompleta + '" target="_blank">' + datos.origen_descripcion + '</a>' +
            '<a href="#" data-target="#exampleModal1" data-toggle="modal"><i class="fa fa-info-circle"></i></a></td>')
        .append('<td>' + datos.destino_descripcion + '</td>')
        .append('<td>' + datos.fecha_hora + '</td>')
        .append('<td class="text-center" id="editarzona' + id + '">' + zona_name + ' ')
        .append('<td class="text-center" id="conductor' + id + '" style="width: 100px;">' + select_conductores + '</td>')
        .append('<td class="text-center" id="estado' + id + '"><span class="badge badge-warning">Pendiente</span></td>')
        .append('<td class="row-actions" id="accion' + id + '">' +
            '<a href="#" class="cambiarEstado" id="' + id + '"><i class="os-icon os-icon-arrow-down4"></i></a>' +
            '<a href="#" class="editarZona" id="' + id + '"><i class="os-icon os-icon-ui-49"></i></a>' +
            '<a href="#" class="cancelar" id="' + id + '"><i class="os-icon os-icon-cancel-circle"></i></a></td>')

    table = $('#myTable').DataTable();
    table.row.add(row).draw(false);

    $('.js-example-basic-single').select2();
};

cambiar_estado = function (datos) {
    var y = document.getElementById("snackbar");
    var x = datos.id;
    //console.log(datos.conductor);

    switch (datos.estado) {
        case ('E'):
            $('#count' + datos.zona).html(datos.contador);
            $('#' + datos.dni).remove();
            $('#estado' + x).html('<span class="badge badge-secondary">En Curso</span>');
            $('.porzona' + datos.dni).remove();
            $('#conductor' + x).html(datos.conductor);
            $('#snackbar').html('El movil ' + datos.conductor + ' fue asignado al pedido ');
            y.className = "show";
            setTimeout(function () {
                $('#accion' + x).html('<a href="#" class="cambiarEstado" id="' + x + '"><i class="os-icon os-icon-arrow-down4"></i></a><a href="#" class="editar" id="' + x + '"><i class="os-icon os-icon-ui-49"></i></a><a href="#" class="cancelar" id="' + x + '"><i class="os-icon os-icon-cancel-circle"></i></a>');
                setTimeout(function () {
                    y.className = y.className.replace("show", "");
                }, 2000);
            }, 500);
            break;

        case ('O'):
            $('#count' + datos.zona).html(datos.contador);
            $('#' + datos.dni).remove();
            $('#estado' + x).html('<span class="badge badge-secondary">Completado</span>');
            $('#encurso' + x).html('<span class="badge badge-secondary">Completado</span>');
            $('#conductor' + x).html(datos.conductor);
            $('#snackbar').html('El movil ' + datos.conductor + ' fue asignado al pedido ');
            y.className = "show";
            setTimeout(function () {
                $('#accion' + x).html('<a href="#" class="cambiarEstado" id="' + x + '"><i class="os-icon os-icon-arrow-down4"></i></a><a href="#" class="editar" id="' + x + '"><i class="os-icon os-icon-ui-49"></i></a><a href="#" class="cancelar" id="' + x + '"><i class="os-icon os-icon-cancel-circle"></i></a>');
                $('#aencurso' + x).html('<a href="#" class="cambiarEstado" id="' + x + '"><i class="os-icon os-icon-arrow-down4"></i></a><a href="#" class="editar" id="' + x + '"><i class="os-icon os-icon-ui-49"></i></a><a href="#" class="cancelar" id="' + x + '"><i class="os-icon os-icon-cancel-circle"></i></a>');
                setTimeout(function () {
                    y.className = y.className.replace("show", "");
                }, 2000);
            }, 500);
            break;

        case ('B'):
            $('#estado' + x).html('<span class="badge badge-primary">Pasajero a Bordo</span>');
            $('#accion' + x).html('<a href="#" class="cancelar" id="' + x + '"><i class="os-icon os-icon-cancel-circle"></i></a>');
            $('#zona' + x).prop("disabled", true);
            break;

        case ('F'):
            $('#estado' + x).html('<span class="badge badge-success">Finalizado</span>');
            $('#completado' + x).html('<span class="badge badge-success">Finalizado</span>');
            $('#zona' + x).prop("disabled", true);
            $('#snackbar').html('El viaje ha finalizado');
            setTimeout(function () {
                $('#accion' + x).html('<td></td>');
                $('#acompletado' + x).html('<td></td>');
                setTimeout(function () {
                    y.className = y.className.replace("show", "");
                }, 2000);
            }, 500);
            break;

        case ('C'):
            $('#estado' + x).html('<span class="badge badge-danger">Cancelado</span>');
            $('#encurso' + x).html('<span class="badge badge-danger">Completado</span>');
            $('#completado' + x).html('<span class="badge badge-danger">Completado</span>');
            $('#zona' + x).prop("disabled", true);
            // console.log(datos);

            row = $("#viaje" + x);
            $(row).prependTo('#myTable');
            $('#accion' + x).html('<td></td>');
            $('#aencurso' + x).html('<td></td>');
            $('#acompletado' + x).html('<td></td>');
            $('#itemes' + datos.zona.id).prepend('<div class="item" id="' + datos.dni_cond + '"><div class="inactivo quitarZona" id="' + datos.dni_cond + '" style="cursor:pointer;">' + datos.movil + '</div></div>');
            $('.zona' + datos.zona.id).prepend('<option value="' + datos.dni_cond + '" class="porzona' + datos.dni_cond + '">' + datos.movil + '</option>');
            break;
    }
};

asignar_zona = function (data) {
    var x = data.zona;
    var y = document.getElementById("snackbar");

    $('#' + data.dni).remove();
    $('.porzona' + data.dni).remove();
    if (data.esAbonado == "true") {
        $('#itemes' + x).append('<div class="item" id="' + data.dni + '"><div class="inactivo quitarZona" id="' + data.dni + '" style="cursor: pointer;">' + data.conductor + '</div></div>');
    } else {
        $('#itemes' + x).append('<div class="itemRojo" id="' + data.dni + '"><div class="inactivo quitarZona" id="' + data.dni + '" style="cursor: pointer;">' + data.conductor + '</div></div>');
    }

    //agregar en el combobox de la lista de pedidos
    $('.zona' + data.zona).append('<option value="' + data.dni + '" class="porzona' + data.dni + '">' + data.conductor + '</option>');
    $('#count' + x).html(data.contador);
    $('#count' + data.zonaAnt).html(data.contAnterior);

    $('#snackbar').html('El movil ' + data.conductor + ' fue asignado a la zona ' + data.zname);
    y.className = "show";
    setTimeout(function () { y.className = y.className.replace("show", ""); }, 3000);
};

quitar_zona = function (data) {
    var x = data.zona;
    var y = document.getElementById("snackbar");
    $('#count' + x).html(data.contador);

    $('#' + data.dni).remove();
    $('.porzona' + data.dni).remove();
    $('#snackbar').html('El conductor ' + data.conductor + ' fue removido de la zona ' + data.zname);
    y.className = "show";
    setTimeout(function () { y.className = y.className.replace("show", ""); }, 3000);
};