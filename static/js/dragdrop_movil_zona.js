var drake = dragula({
    moves: (el, source, handle, sibling) => !el.classList.contains('no-drag'),
    isContainer: function (el) {
        return el.classList.contains('itemes');
    }
});

drake.on('drop', function (el, target, source, sibling) {
    var elementId = el.id;
    var elementId2 = sibling.id;
    cambiarOrden(elementId, elementId2);
});

function cambiarOrden(elementId, otroId) {
    var baseUrl = $('#baseUrl').val();
    // update the object here, for example:
    $.ajax({
        data: { 'id_1': elementId, 'id_2': otroId },
        url: '/cambiar/',
        type: 'get',
        cache: false,
        success: function (data) {
            //console.log("cambio de orden. OK");
        },
        fail: function (data) {
            console.log("Error: " + data);
        }
    });
    return false;
}