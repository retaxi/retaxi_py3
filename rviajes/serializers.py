from .models import Rule, RViaje
from rest_framework import serializers


class RuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rule
        fields = ('name',
        	'description', 
        	'frequency',
            'params'
            )


class RViajeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RViaje
        fields = (
            'start',
            'end',
        	'name',
            'creator',
            'rule',
            'pasajero',
            'conductor',
            'origen',
            'destino',
            'origen_descripcion',
            'destino_descripcion',
            'state',
            'zona',
            'zona_return',
            'round_trip',
            'start_return',
            'end_return' )