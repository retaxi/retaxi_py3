from dateutil import rrule
from dateutil.rrule import (DAILY, FR, HOURLY, MINUTELY, MO, MONTHLY, SA,
                            SECONDLY, SU, TH, TU, WE, WEEKLY, YEARLY)
from datetime import datetime

from django.conf import settings
from django.contrib.gis.db import models

from conductores.models import Conductor
from pasajeros.models import Pasajero
from zona.models import Zona

freq_dict_order = {
    'YEARLY': 0,
    'MONTHLY': 1,
    'WEEKLY': 2,
    'DAILY': 3,
    'HOURLY': 4,
    'MINUTELY': 5,
    'SECONDLY': 6
}

freqs = (   ("YEARLY", "Anual"),
            ("MONTHLY", "Mensual"),
            ("WEEKLY", "Semanal"),
            ("DAILY", "Diario"),
            ("HOURLY", "Por Hora"),
            ("MINUTELY", "Por Minuto"),
            ("SECONDLY", "Por Segundo"))


param_dict_order = {
    'byyearday': 1,
    'bymonth': 1,
    'bymonthday': 2,
    'byweekno': 2,
    'byweekday': 3,
    'byhour': 4,
    'byminute': 5,
    'bysecond': 6
}

class Rule(models.Model):
    """
    This defines a rule by which an event will recur.  This is defined by the
    rrule in the dateutil documentation.
    * name - the human friendly name of this kind of recursion.
    * description - a short description describing this type of recursion.
    * frequency - the base recurrence period
    * param - extra params required to define this type of recursion. The params
      should follow this format:
        param = [rruleparam:value;]*
        rruleparam = see list below
        value = int[,int]*
      The options are: (documentation for these can be found at
      http://labix.org/python-dateutil#head-470fa22b2db72000d7abe698a5783a46b0731b57)
        ** count
        ** bysetpos
        ** bymonth
        ** bymonthday
        ** byyearday
        ** byweekno
        ** byweekday
        ** byhour
        ** byminute
        ** bysecond
        ** byeaster
    """
    name = models.CharField("nombre", max_length=32)
    description = models.TextField("descripción")
    frequency = models.CharField("frequencia", choices=freqs, max_length=10)
    params = models.TextField("parámetros", null=True, blank=True)

    class Meta:
        verbose_name = 'regla'
        verbose_name_plural = 'reglas'

    def rrule_frequency(self):
        compatibility_dict = {
            'DAILY': DAILY,
            'MONTHLY': MONTHLY,
            'WEEKLY': WEEKLY,
            'YEARLY': YEARLY,
            'HOURLY': HOURLY,
            'MINUTELY': MINUTELY,
            'SECONDLY': SECONDLY
        }
        return compatibility_dict[self.frequency]

    def get_params(self):
        """
        >>> rule = Rule(params = "count:1;bysecond:1;byminute:1,2,4,5")
        >>> rule.get_params()
        {'count': 1, 'byminute': [1, 2, 4, 5], 'bysecond': 1}
        """
        if self.params is None:
            return {}
        params = self.params.split(';')
        param_dict = []
        for param in params:
            param = param.split(':')
            if len(param) == 2:
                param = (str(param[0]), [int(p) for p in param[1].split(',')])
                if len(param[1]) == 1:
                    param = (param[0], param[1][0])
                param_dict.append(param)
        return dict(param_dict)

    def __unicode__(self):
        """Human readable string for Rule"""
        return self.name


class Holiday(models.Model):
    name = models.CharField("nombre", max_length=32, blank=False, null=False)
    date = models.DateField("fecha", blank=False, null=False)

    class Meta:
        verbose_name = 'feriado'
        verbose_name_plural = 'feriados'

    def __str__(self):
        return super(Holiday, self).__str__()


class RViaje(models.Model):
    states = (  ('A', 'Activo'),
                ('C', 'Cancelado'),
                ('P', 'Pausado'))
    
    start = models.DateTimeField("primero")
    end = models.DateTimeField("último")
    name = models.CharField("nombre", max_length = 255)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, null=True, editable=False, verbose_name="creador")
    created_on = models.DateTimeField("creado", auto_now_add=True)
    updated_on = models.DateTimeField("actualizado", auto_now=True)
    rule = models.ForeignKey(Rule, on_delete=models.DO_NOTHING, null = True, blank = True, verbose_name="regla", help_text="Seleccione '----' por viaje diferido")
    pasajero = models.ForeignKey(Pasajero, blank=True, null=True, on_delete=models.DO_NOTHING)
    conductor = models.ForeignKey(Conductor, blank=True, null=True, on_delete=models.DO_NOTHING)
    state = models.CharField("estado", choices=states, max_length=1)
    origen = models.PointField(null=True, blank=True, default='POINT(0 0)', srid=4326)
    destino = models.PointField(null=True, blank=True, default='POINT(0 0)', srid=4326)
    origen_descripcion = models.CharField(max_length=100, null=False, blank=False)
    destino_descripcion = models.CharField(max_length=100, null=True, blank=True)    
    zona = models.ForeignKey(Zona, blank=True, null=True, on_delete=models.DO_NOTHING, related_name="zona")
    zona_return = models.ForeignKey(Zona, blank=True, null=True, on_delete=models.DO_NOTHING, related_name="zona_return")
    round_trip = models.BooleanField("ida y vuelta")
    start_return = models.DateTimeField("primer regreso")
    end_return = models.DateTimeField("último regreso")

    class Meta:
        verbose_name = "viaje recurrente"
        verbose_name_plural = "viajes recurrentes"

    def __str__(self):
        return super(RViaje, self).__str__()

    
    @property
    def start_params(self):
        start = self.start
        params = {
            'byyearday': start.timetuple().tm_yday,
            'bymonth': start.month,
            'bymonthday': start.day,
            'byweekno': start.isocalendar()[1],
            'byweekday': start.weekday(),
            'byhour': start.hour,
            'byminute': start.minute,
            'bysecond': start.second
        }
        return params


    @property
    def rule_params(self):
        return self.rule.get_params()


    def _event_params(self):
        freq_order = freq_dict_order[self.rule.frequency]
        rule_params = self.rule_params
        start_params = self.start_params
        event_params = {}

        if len(rule_params) == 0:
            return event_params

        for param in rule_params:
            # start date influences rule params
            if (param in param_dict_order and param_dict_order[param] > freq_order and
                    param in start_params):
                sp = start_params[param]
                if sp == rule_params[param] or (
                        hasattr(rule_params[param], '__iter__') and
                        sp in rule_params[param]):
                    event_params[param] = [sp]
                else:
                    event_params[param] = rule_params[param]
            else:
                event_params[param] = rule_params[param]

        return event_params


    def get_rrule_object(self):
        if self.rule is None:
            return
        params = self.rule.get_params()
        frequency = self.rule.rrule_frequency()
        dtstart = self.start

        if self.end is None:
            until = None
        else:
            until = self.end
        
        return rrule.rrule(frequency, dtstart=dtstart, until=until, **params)