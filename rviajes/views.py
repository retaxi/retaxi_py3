from .models import RViaje, Rule
from rest_framework import generics
from .serializers import RViajeSerializer, RuleSerializer


class RulesList(generics.ListAPIView):
    queryset = Rule.objects.all()
    serializer_class = RuleSerializer


class RViajesList(generics.ListAPIView):
    queryset = RViaje.objects.all()
    serializer_class = RViajeSerializer


# Crear API para el ema
#def crear_viajes:
#    pass