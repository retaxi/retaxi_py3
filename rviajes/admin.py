from django.contrib import admin
from .models import *
from viajes.models import Viaje

def mostrar_viajes(self, request, queryset):
    print(queryset)
    fechas = list(queryset[0].get_rrule_object())

    self.message_user(request, "%s" % fechas)

mostrar_viajes.short_description = "Mostrar los Viajes"

# ocultar un campo cuando otro es true
class MenuRViajeAdmin(admin.ModelAdmin):
    list_display = ['name', 'start', 'end']
    actions=[mostrar_viajes]

    class Meta:
        model = RViaje 

    """ def get_form(self, request, obj=None, **kwargs):
        self.exclude = []
        if obj and not obj.round_trip:
            self.exclude.append('start_return')
            self.exclude.append('end_return')
        return super(MenuRViajeAdmin, self).get_form(request, obj, **kwargs) """

    def save_model(self, request, obj, form, change):
        obj.creator = request.user
        super().save_model(request, obj, form, change)


class RuleAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'frequency']
    list_filter = ['name', 'description', 'frequency']

    class Meta:
        model = Rule


class HolidayAdmin(admin.ModelAdmin):
    list_display = ['name', 'date']
    list_filter = ['name', 'date']

    class Meta:
        model = Holiday


admin.site.register(RViaje, MenuRViajeAdmin)
admin.site.register(Rule, RuleAdmin)
admin.site.register(Holiday, HolidayAdmin)

