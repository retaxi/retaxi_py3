from django.apps import AppConfig


class RviajesConfig(AppConfig):
    name = 'Viajes Recurrentes'
