# Generated by Django 2.0.7 on 2018-09-28 20:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viajes', '0004_viaje_zona'),
    ]

    operations = [
        migrations.AddField(
            model_name='viaje',
            name='tipo',
            field=models.CharField(blank=True, choices=[('T', 'Telefono'), ('W', 'WhatsApp')], default='T', max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='viaje',
            name='estado',
            field=models.CharField(blank=True, choices=[('P', 'Pendiente'), ('E', 'En Curso'), ('B', 'Pasajero a Bordo'), ('F', 'Finalizado'), ('C', 'Cancelado')], default='P', max_length=1, null=True),
        ),
    ]
