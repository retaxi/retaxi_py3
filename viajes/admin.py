from django.contrib import admin
from .models import Viaje, Calificacion

# Register your models here.
class ViajeAdmin(admin.ModelAdmin):
    list_display = ['id', 'conductor', 'pasajero', 'origen', 'destino', 'origen_descripcion',
                    'destino_descripcion', 'fecha_hora', 'fecha_hora_inicio', 'fecha_hora_fin']
    search_fields = ['conductor', 'pasajero', 'origen', 'destino', 'origen_descripcion',
                        'destino_descripcion', 'fecha_hora_inicio', 'fecha_hora', 'fecha_hora_fin']
    list_filter = ['conductor', 'pasajero', 'origen', 'destino', 'origen_descripcion',
                    'destino_descripcion', 'fecha_hora_inicio', 'fecha_hora', 'fecha_hora_fin']

    ordering = ['id', 'conductor', 'pasajero', 'origen', 'destino', 'origen_descripcion',
                    'destino_descripcion', 'fecha_hora_inicio', 'fecha_hora', 'fecha_hora_fin']

    class Meta:
        model = Viaje


class CalificacionAdmin(admin.ModelAdmin):
    list_display = ['viaje_fecha_hora', 'conductor', 'pasajero', 'puntos_conductor', 'puntos_base', 'comentario']
    search_fields = ['viaje_fecha_hora', 'conductor', 'pasajero', 'puntos_conductor', 'puntos_base', 'comentario']
    list_filter = ['viaje', 'conductor', 'pasajero', 'puntos_conductor', 'puntos_base', 'comentario']
    ordering = ['viaje', 'conductor', 'pasajero', 'puntos_conductor', 'puntos_base', 'comentario']

    def viaje_fecha_hora(self, obj):
        return obj.viaje.fecha_hora

    class Meta:
        model = Calificacion 


admin.site.register(Viaje, ViajeAdmin)
admin.site.register(Calificacion, CalificacionAdmin)