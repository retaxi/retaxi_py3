from .models import Viaje
from rest_framework import serializers
from pasajeros.serializers import UserPasajeroSerializer
from conductores.serializers import UserConductorSerializer, ConductorSerializer
from conductores.models import Conductor
from users.models import Userthumb
from rest_framework.authtoken.models import Token
from datetime import datetime


class ViajeSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    pasajero = UserPasajeroSerializer(many=False, read_only=True)
    conductor = UserConductorSerializer(many=False, read_only=True)

    class Meta:
        model = Viaje

        fields = (
            'id',
            'conductor',
            'pasajero',
            'fecha_hora',
            'origen',
            'destino',
            'origen_descripcion',
            'destino_descripcion',
            'is_diferido',
            'comentario')


class PedidoSerializer(serializers.ModelSerializer):
    nombre_conductor = serializers.SerializerMethodField()
    dni_cond = serializers.SerializerMethodField()
    nombre_pasajero = serializers.SerializerMethodField()
    img_pasajero = serializers.SerializerMethodField()
    cel_pasajero = serializers.SerializerMethodField()
    movil = serializers.SerializerMethodField()
    fecha_hora = serializers.DateTimeField(format="%H:%M")
    conductores_zona = serializers.SerializerMethodField()
    conductores_all = serializers.SerializerMethodField()
    nombre_zona = serializers.SerializerMethodField()

    def get_dni_cond(self, obj):
        if obj.conductor is not None:
            return obj.conductor.dni

        return None

    def get_nombre_conductor(self, obj):
        return str(obj.conductor)

    def get_nombre_zona(self, obj):
        if obj.zona:
            return str(obj.zona.number)
        return 'Sin Zona'

    def get_movil(self, obj):
        if obj.conductor:
            return str(obj.conductor.registro)
        return ''

    def get_nombre_pasajero(self, obj):
        return str(obj.pasajero)

    def get_cel_pasajero(self, obj):
        if obj.pasajero:
            return str(obj.pasajero.telephone)
        return ''

    def get_img_pasajero(self, obj):
        if obj.pasajero:
            if obj.pasajero.user:
                try:
                    userthumb = Userthumb.objects.get(user=obj.pasajero.user)
                    return userthumb.social_thumb
                except Userthumb.DoesNotExist:
                    return ''
        return ''

    def get_conductores_zona(self, obj):
        conductores = Conductor.objects.filter(zona=obj.zona)
        return ConductorSerializer(conductores, many=True).data

    def get_conductores_all(self, obj):
        conductores = Conductor.objects.filter(estado='a')
        return ConductorSerializer(conductores, many=True).data

    class Meta:
        model = Viaje

        fields = (
            'id',
            'dni_cond',
            'nombre_conductor',
            'movil',
            'nombre_pasajero',
            'cel_pasajero',
            'img_pasajero',
            'fecha_hora',
            'origen',
            'destino',
            'origen_descripcion',
            'destino_descripcion',
            'estado',
            'nombre_zona',
            'conductores_zona',
            'conductores_all',
            'tipo',
            'is_diferido',
            'comentario')
        depth = 2
