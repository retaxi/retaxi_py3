from django_celery_beat.models import CrontabSchedule, PeriodicTask
from django.conf import settings

MINUTOS_AVISO = 15 

def cargar_tarea_viaje(viaje):
    fecha_hora = viaje.fecha_hora - datetime.timedelta(minutes=MINUTOS_AVISO)

    schedule, _ = CrontabSchedule.objects.get_or_create(
        minute=str(fecha_hora.time(), 'M'),
        hour=str(fecha_hora.time(), 'H'),
        day_of_week='*',
        day_of_month=str(fecha_hora.time(), 'd'),
        month_of_year=str(fecha_hora.time(), 'm'),
        timezone=pytz.timezone(settings.TIME_ZONE)
    )

    fecha_fin = fecha_hora + datetime.timedelta(hours=1)

    PeriodicTask.objects.create(
        crontab=schedule,
        name='Activar Viaje ' + viaje.id,
        task='viajes.tasks.activar_viaje',
        args=(viaje.id,),
        options={expires:fecha_fin}
    )