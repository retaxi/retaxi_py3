from channels.generic.websocket import AsyncJsonWebsocketConsumer
import json

class PedidosConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        # La sala es única para los pedidos
        self.room_name = 'pedidos'
        self.room_group_name = 'pedidos'

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):

        # Send message to WebSocket
        await self.send_json(
            {
                'message': event['message'],
            }
        )


    async def enviar(text_data):
        #text_data_json = json.loads(text_data)
        #message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': text_data
            }
        )