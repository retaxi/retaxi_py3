from django.conf import settings
from django.contrib.gis.db import models
from django.utils import timezone
from conductores.models import Conductor
from pasajeros.models import Pasajero
from rviajes.models import RViaje
from zona.models import Zona
from django.dispatch import receiver
from django.db.models.signals import post_save
from datetime import datetime
from django.core import serializers


class Viaje(models.Model):
    STATUS = (
        ('P', 'Pendiente'),
        ('O', 'Completado'),
        ('E', 'En Curso'),
        ('B', 'Pasajero a Bordo'),
        ('F', 'Finalizado'),
        ('C', 'Cancelado')
    )
    TIPO = (
        ('M', 'Móvil'),
        ('T', 'Telefono'),
        ('W', 'WhatsApp'),
        ('D', 'Diferido'),
    )

    id = models.AutoField(primary_key=True)
    conductor = models.ForeignKey(
        Conductor, blank=True, null=True, on_delete=models.DO_NOTHING)
    pasajero = models.ForeignKey(
        Pasajero, blank=True, null=True, on_delete=models.DO_NOTHING)
    fecha_hora = models.DateTimeField(default=timezone.now,)
    origen = models.PointField(
        null=True, blank=True, default='POINT(0 0)', srid=4326)
    destino = models.PointField(
        null=True, blank=True, default='POINT(0 0)', srid=4326)
    fecha_hora_fin = models.DateTimeField(null=True)
    origen_descripcion = models.CharField(
        max_length=100, null=True, blank=True)
    destino_descripcion = models.CharField(
        max_length=100, null=True, blank=True)
    fecha_hora_inicio = models.DateTimeField(null=True)
    estado = models.CharField(
        max_length=1, choices=STATUS, default='P', null=True, blank=True)
    tipo = models.CharField(max_length=1, choices=TIPO,
                            default='M', null=True, blank=True)
    operador = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING,
                                 null=True, blank=True, related_name='viaje_operador')
    receptor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING,
                                 null=True, blank=True, editable=False, related_name='viaje_receptor')
    is_diferido = models.BooleanField(null=False, blank=False, default=False)
    comentario = models.CharField(max_length=60, null=True, blank=True)
    zona = models.ForeignKey(Zona, blank=True, null=True,
                             on_delete=models.DO_NOTHING)

    rviaje = models.ForeignKey(RViaje, on_delete=models.DO_NOTHING, null=True,
                               blank=True, editable=False, related_name='viaje_recurrente')

    class Meta:
        verbose_name = "viaje"
        verbose_name_plural = "viajes"

    def __str__(self):
        return super(Viaje, self).__str__()


class Calificacion(models.Model):
    conductor = models.ForeignKey(
        Conductor, blank=True, null=True, on_delete=models.DO_NOTHING)
    pasajero = models.ForeignKey(
        Pasajero, blank=True, null=True, on_delete=models.DO_NOTHING)
    viaje = models.ForeignKey(
        Viaje, blank=True, null=True, on_delete=models.DO_NOTHING)
    puntos_conductor = models.FloatField(blank=True, null=True, default=1)
    puntos_base = models.FloatField(blank=True, null=True, default=1)
    comentario = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        verbose_name = "Calificación"
        verbose_name_plural = "Calificaciones"

    def __str__(self):
        return super(Calificacion, self).__str__()


@receiver(post_save, sender=RViaje)
def crear_viajes(sender, instance, created, **kwargs):
    hora_ida = instance.start.time()
    fechas = list(instance.get_rrule_object())

    if created:
        v = []
        for f in fechas:
            v = Viaje.objects.create(conductor=instance.conductor,
                                     origen=instance.origen,
                                     destino=instance.destino,
                                     pasajero=instance.pasajero,
                                     origen_descripcion=instance.origen_descripcion,
                                     destino_descripcion=instance.destino_descripcion,
                                     fecha_hora=datetime.combine(f, hora_ida),
                                     fecha_hora_inicio=None,
                                     fecha_hora_fin=None,
                                     zona=instance.zona,
                                     tipo='D',
                                     operador=None,
                                     receptor=None,
                                     estado='P',
                                     is_diferido=True,
                                     rviaje=instance)

            # assuming obj is a model instance
            #serialized_obj = serializers.serialize('json', [ v, ])
            # print(serialized_obj)

        # Funca bien usar creted para crear y sino actualizar todos los viajes del mismo RViaje, q estan pendientes.
        # Por ahora trataría la creación no más
        # TRATAR LA VUELTAAAA !!!
