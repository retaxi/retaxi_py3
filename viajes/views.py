# -*- coding: utf-8 -*-
from django.http import HttpResponseServerError
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point, fromstr
from django.contrib.gis.measure import D
from django.views.decorators.http import condition
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from pyfcm import FCMNotification
from pyfcm.errors import FCMError
from rest_framework import authentication, permissions, generics, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from viajes import *
from .serializers import ViajeSerializer, PedidoSerializer
from zona.serializers import ZonaSerializer
from conductores.serializers import MovilSerializer
from .models import Viaje, Pasajero, Calificacion
from conductores.models import Zona
from conductores.models import Conductor, Movil
from tarifas.views import CalcularTarifa
from direcciones.views import getLatLng
from datetime import timedelta, datetime
import logging
import json

log = logging.getLogger(__name__)

push_service = FCMNotification(api_key=settings.FIREBASE_API_KEY)

SEGUNDOS = 15
RADIUS = 3
FECHA_CANCEL = datetime(2000, 1, 1)


class BaseViajesView(generics.GenericAPIView):
    serializer_class = None
    permission_classes = (permissions.IsAuthenticated, )
    authentication_classes = (authentication.TokenAuthentication,)

    def resultado_solicitud(self, result, objects):
        """
        :param result: resultado del servico FCM
        :type objects: Pasajero o Conductor a cambiar firebase_token
        """
        if result['failure'] == 0 and result['canonical_ids'] == 0:
            return Response({MESSAGE_RESPUESTA: SOLICITUD_ENVIADA})
        else:
            i = 0
            error = ''
            for r in result['results']:
                if 'message_id' in r:
                    if 'registration_id' in r:
                        new_token = r['registration_id']
                        if type(objects) == 'list':
                            o = objects[i]
                        else:
                            o = objects
                        o.firebase_token = new_token
                        o.save()
                else:
                    if 'error' in r:
                        error += '/n' + r['error']
                i += 1

            if result['success'] == 0:
                # todos fallidos
                return self.respond_error(error)
            elif len(error) > 0:
                self.log_exception(error)

            if result['success'] > 0:
                # OK
                return Response({MESSAGE_RESPUESTA: SOLICITUD_ENVIADA})

    def respond_error(self, error):
        if isinstance(error, Exception):
            self.log_exception(error)
        else:
            log.error(error)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def log_exception(self, error):
        if getattr(error, 'response', None) is not None:
            if type(error) == 'str':
                err_msg = error
            else:
                err_msg = error.args[0]
            try:
                err_data = error.response.json()
            except (ValueError, AttributeError):
                log.error(u'%s; %s', error, err_msg)
            else:
                log.error(u'%s; %s; %s', error, err_msg, err_data)
        else:
            log.exception(u'%s; %s', error, str(error))


class PedirRemisView(BaseViajesView):
    """
    Vista para que un pasajero pueda solicitar remises

    **Input**:

        {
            "origen": "23.8, 22.3",
            "destino": "20.0, -23.0",
            "origen_descripcion": "Alem 185",
            "destino_descripcion": "Terminal de Omnibus",
            "fecha_hora": "01/10/2018 01:30:00.000",
            "comentario": <comentario del viaje>
        }

    **Output**:

    viaje data in serializer_class format
    """

    def post(self, request, *args, **kwargs):
        """ Pedir Remis:
            1 - Busco los ultimos conductores que actualizaron sus posiciones
            2 - filtro los mas cercanos al pasajero (que caen dentro de un radio)
            3 - Envío mensaje al conductor avisando del pedido
            4 - Si acepta el viaje termina, sino selecciona el siguiente conductor mas cercano y vuelve al punto 3"""

        input_data = request.data.copy()

        pasajero = Pasajero.objects.get(user=request.user)
        origen_a = input_data['origen'].split(',')
        origen = Point(float(origen_a[0]), float(origen_a[1]), srid=4326)
        origen_desc = input_data['origen_descripcion']

        comentario = ''
        if 'comentario' in input_data:
            comentario = input_data['comentario']

        if 'fecha_hora' in input_data:
            fecha_hora = datetime.strptime(
                input_data['fecha_hora'], "%d/%m/%Y %H:%M")
            is_diferido = True
        else:
            fecha_hora = datetime.now()
            is_diferido = False

        destino = None
        destino_desc = ""
        if 'destino' in input_data and input_data['destino'] != '':
            destino_a = input_data['destino'].split(',')
            destino = Point(float(destino_a[0]), float(
                destino_a[1]), srid=4326)
            destino_desc = input_data['destino_descripcion']

        fecha_hasta = datetime.now()
        fecha_desde = fecha_hasta - timedelta(seconds=SEGUNDOS)

        # Cancelo todos los viajes por las dudas que haya quedado alguno
        viaje = None
        viajes = Viaje.objects.filter(
            pasajero=pasajero, fecha_hora_fin=None, is_diferido=is_diferido)
        if len(viajes) > 0:
            viaje = viajes[0]

        if ASIGNA_AUTOMATICO:
            # Obtengo los conductores mas cercanos al pasajero
            conductores = Conductor.objects.filter(fecha_ultima_ubicacion__gte=fecha_desde, ultima_ubicacion__distance_lt=(
                origen, D(km=RADIUS)), estado='a', abonado='s')

            if len(conductores) == 0:
                if settings.DEBUG:
                    return Response({MESSAGE_RESPUESTA: CONDUCTORES_NO_DISPONIBLES})

                return Response({MESSAGE_RESPUESTA: CONDUCTORES_NO_DISPONIBLES})

            # Agrego campo distancia para poder ordenar
            conductores = conductores.annotate(
                distance=Distance('ultima_ubicacion', origen))
            # Ordeno por distancia ascendente
            conductores = conductores.order_by('distance')

            try:
                tarifa = CalcularTarifa(
                    input_data['origen'], input_data['destino'])
            except Exception as e:
                return self.respond_error(e)

            # Armo mensaje para conductores con datos del pasajero + origen y destino
            viaje = Viaje.objects.create(
                conductor=None, pasajero=pasajero, origen=origen, destino=destino,
                origen_descripcion=origen_desc, destino_descripcion=destino_desc,
                fecha_hora=fecha_hora, is_diferido=is_diferido, tipo='M', comentario=comentario)

            serializer = ViajeSerializer(viaje)

            data_message = {
                MESSAGE_TYPE: MESSAGE_TYPE_TAXI_REQUEST,
                'viaje': serializer.data,
                'tarifa': tarifa
            }

            registration_ids = []
            for c in conductores:
                registration_ids.append(c.firebase_token)

            try:
                result = push_service.notify_multiple_devices(
                    registration_ids=registration_ids, data_message=data_message)
            except FCMError as e:
                return self.respond_error(e)

            return self.resultado_solicitud(result, conductores)

        else:
            if viaje is None:
                try:
                    ori = fromstr(
                        'POINT(' + origen_a[1] + ' ' + origen_a[0] + ')')
                    zona = Zona.objects.get(poli__intersects=ori, hora_desde__lte=fecha_hora.time(
                    ), hora_hasta__gte=fecha_hora.time())
                    destino_desc = destino_desc.split(',')[0]
                    origen_desc = origen_desc.split(',')[0]
                except Zona.DoesNotExist:
                    # self.log_exception(self, 'Origen sin ZONA: ' + origen)
                    zona = None

                viaje = Viaje.objects.create(
                    conductor=None, pasajero=pasajero, origen=origen, destino=destino, origen_descripcion=origen_desc,
                    destino_descripcion=destino_desc, fecha_hora=fecha_hora, zona=zona, is_diferido=is_diferido, tipo='M', comentario=comentario)

                serializer = PedidoSerializer(viaje)

                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.group_send)(
                    "pedidos", {"type": "chat_message", "message": serializer.data})
                async_to_sync(channel_layer.group_send)("chat", {
                    "type": "chat.nuevoviaje", "room_id": pasajero.pk, "username": "", "message": serializer.data})

            return Response({MESSAGE_RESPUESTA: SOLICITUD_ENVIADA, "data": str(viaje.id)})


class AceptarViajeView(BaseViajesView):
    """
    Vista para que un conductor pueda aceptar solicitud de viaje

    **Input**:

        {
            "origen": "23.8, 22.3",
            "destino": "20.0, -23.0",
            "origen_descripcion": "Alem 185",
            "destino_descripcion": "Terminal de Omnibus"
            "pasajero": 3 (dni del pasajero),
            "movil": 1 (id movil)
        }

    **Output**:

    viaje data in serializer_class format
    """

    def post(self, request, *args, **kwargs):
        input_data = request.data.copy()

        conductor = Conductor.objects.get(user=request.user)
        pasajero = Pasajero.objects.get(user=input_data['pasajero'])
        movil = input_data['movil']
        origen_a = input_data['origen'].split(',')
        destino_a = input_data['destino'].split(',')
        origen = Point(float(origen_a[0]), float(origen_a[1]), srid=4326)
        destino = Point(float(destino_a[0]), float(destino_a[1]), srid=4326)

        if 'origen_descripcion' in input_data:
            origen_desc = input_data['origen_descripcion']
        if 'destino_descripcion' in input_data:
            destino_desc = input_data['destino_descripcion']

        viajes_activos = Viaje.objects.filter(
            pasajero=pasajero, fecha_hora_fin=None).exclude(conductor=None)
        if len(viajes_activos) > 0:
            data = {
                MESSAGE_RESPUESTA: VIAJE_ACEPTADO_POR_OTRO_CONDUCTOR
            }
            return Response(data)

        try:
            tarifa = CalcularTarifa(
                input_data['origen'], input_data['destino'])
        except Exception as e:
            return self.respond_error(e)

        # Armo mensaje para conductores condatos del pasajero + origen y destino
        # viaje = Viaje(conductor=conductor, pasajero=pasajero, origen=origen, destino=destino,
        #                              origen_descripcion=origen_desc, destino_descripcion=destino_desc)
        # viaje.save()
        try:
            viaje = Viaje.objects.get(
                conductor=None, pasajero=pasajero, fecha_hora_fin=None)
            if viaje:
                viaje.conductor = conductor
                viaje.save()
        except Viaje.DoesNotExist:
            data = {
                MESSAGE_RESPUESTA: VIAJE_PRE_CANCELADO
            }
            return Response(data)

        serializer = ViajeSerializer(viaje)

        data_message = {
            MESSAGE_TYPE: MESSAGE_TYPE_REQUEST_ACCEPTED,
            'viaje': serializer.data,
            'tarifa': tarifa,
            'movil': movil
        }

        registration_id = pasajero.firebase_token
        try:
            result = push_service.notify_single_device(
                registration_id=registration_id, data_message=data_message)
        except FCMError as e:
            return self.respond_error(e)

        return self.resultado_solicitud(result, pasajero)


class CancelarViajeView(BaseViajesView):
    """
    Vista para que un conductor pueda cancelar solicitud de viaje

    **Input**:
        {
            "viaje": <viaje_id>
        }

    **Output**:
        { "respuesta": [USER_INVALID|VIAJE_CANCELADO|ERROR_CANCELA_VIAJE] }
    """

    def post(self, request, *args, **kwargs):
        input_data = request.data.copy()
        viaje_id = input_data['viaje']
        try:
            conductor = Conductor.objects.get(user=request.user)
            if conductor:
                message = VIAJE_CANCELADO_CONDUCTOR
                viaje = Viaje.objects.get(id=viaje_id, conductor=conductor)
        except Conductor.DoesNotExist:
            viaje = None
        except Viaje.DoesNotExist:
            viaje = None

        if viaje is None:
            try:
                pasajero = Pasajero.objects.get(user=request.user)
                if pasajero:
                    message = VIAJE_CANCELADO_PASAJERO
                    viaje = Viaje.objects.get(id=viaje_id, pasajero=pasajero)
            except Pasajero.DoesNotExist:
                return Response({MESSAGE_RESPUESTA: USER_INVALID})
            except Viaje.DoesNotExist:
                return Response({MESSAGE_RESPUESTA: ERROR_CANCELA_VIAJE})

        if viaje:
            data = {
                MESSAGE_TYPE: message
            }
            if message == VIAJE_CANCELADO_PASAJERO:
                o = viaje.conductor
            else:
                o = viaje.pasajero

            if o and o.firebase_token is not None:
                try:
                    result = push_service.notify_single_device(
                        registration_id=o.firebase_token, data_message=data)
                    self.resultado_solicitud(result, o)
                except FCMError as e:
                    return self.respond_error(e)

            viaje.estado = 'C'
            viaje.fecha_hora_fin = FECHA_CANCEL
            viaje.save()

            serializer = PedidoSerializer(viaje)

            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "pedidos", {"type": "chat_message", "message": serializer.data})
            async_to_sync(channel_layer.group_send)("chat", {
                "type": "chat.nuevoviaje", "room_id": viaje.pasajero.pk, "username": "", "message": serializer.data})

            return Response({MESSAGE_RESPUESTA: VIAJE_CANCELADO})
        else:
            return Response({MESSAGE_RESPUESTA: ERROR_CANCELA_VIAJE})


@api_view(['GET'])
def ViajeListPasajero(request):
    input_data = request.query_params.copy()
    estado = input_data['estado']
    is_diferido = bool(int(input_data['diferido']))

    try:
        pasajero = Pasajero.objects.get(user=request.user)
        if pasajero:
            viajes_activos = Viaje.objects.filter(
                pasajero=pasajero, estado=estado, is_diferido=is_diferido).order_by('-fecha_hora')
    except Pasajero.DoesNotExist:
        return Response({MESSAGE_RESPUESTA: USER_INVALID})

    serializer = ViajeSerializer(viajes_activos, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def ViajeListConductor(request):
    input_data = request.query_params.copy()
    estado = input_data['estado']
    is_diferido = bool(int(input_data['diferido']))

    try:
        conductor = Conductor.objects.get(user=request.user)
        if conductor:
            viajes_activos = Viaje.objects.filter(
                conductor=conductor, estado=estado, is_diferido=is_diferido).order_by('-fecha_hora')
    except Conductor.DoesNotExist:
        return Response({MESSAGE_RESPUESTA: USER_INVALID})

    serializer = ViajeSerializer(viajes_activos, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def iniciarViaje(request):
    try:
        conductor = Conductor.objects.get(user=request.user)
        if conductor:
            viajes_activos = Viaje.objects.filter(
                conductor=conductor, estado__in=('E', 'O'))
    except Conductor.DoesNotExist:
        viajes_activos = []
        # return Response({MESSAGE_RESPUESTA: USER_INVALID})

    if len(viajes_activos) == 0:
        try:
            pasajero = Pasajero.objects.get(user=request.user)
            if pasajero:
                viajes_activos = Viaje.objects.filter(
                    pasajero=pasajero, estado__in=('E', 'O'))
        except Pasajero.DoesNotExist:
            return Response({MESSAGE_RESPUESTA: USER_INVALID})

    for viaje in viajes_activos:
        viaje.estado = 'B'
        viaje.fecha_hora_inicio = datetime.now()
        viaje.save()

    serializer = PedidoSerializer(viajes_activos[0])

    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "pedidos", {"type": "chat_message", "message": serializer.data})
    async_to_sync(channel_layer.group_send)("chat", {
        "type": "chat.nuevoviaje", "room_id": viajes_activos[0].pasajero.pk, "username": "", "message": serializer.data})

    return Response({MESSAGE_RESPUESTA: INICIO_VIAJE})


@api_view(['POST'])
def finalizarViaje(request):
    try:
        conductor = Conductor.objects.get(user=request.user)
        if conductor:
            viajes_activos = Viaje.objects.filter(
                conductor=conductor).exclude(estado='F')
    except Conductor.DoesNotExist:
        viajes_activos = []

    if len(viajes_activos) == 0:
        try:
            pasajero = Pasajero.objects.get(user=request.user)
            viajes_activos = Viaje.objects.filter(
                pasajero=pasajero).exclude(estado='F')
        except Pasajero.DoesNotExist:
            return Response({MESSAGE_RESPUESTA: USER_INVALID})

    for viaje in viajes_activos:
        viaje.estado = 'F'
        viaje.fecha_hora_fin = datetime.now()
        viaje.save()

    serializer = PedidoSerializer(viajes_activos[0])

    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "pedidos", {"type": "chat_message", "message": serializer.data})
    async_to_sync(channel_layer.group_send)("chat", {
        "type": "chat.nuevoviaje", "room_id": viajes_activos[0].pasajero.pk, "username": "", "message": serializer.data})

    return Response({MESSAGE_RESPUESTA: FIN_VIAJE})


@api_view(['POST'])
def puntuar_conductor(request):
    """
        Vista para que un coondcutor/pasajero pueda finalizar viaje

            Para Pasajero.
           **Input**:
               {
                   "viaje": <id>
                   "conductor": <dni>
                   "puntos_conductor": ##
                   "puntos_base": ##
                   "comentario": <lo_q_me_quieras_decir>
               }

           **Output**:
            {
                'respuesta': <code>
            }
            code: 14: Error - No se puede punutar xq el Viaje no ha comenzado
                    -1: Error - Usuario inválido
                    22: Viaje Finalizado correctamente
        """

    input_data = request.data.copy()
    viaje_id = input_data['viaje']
    dni_conductor = input_data['conductor']
    puntos_c = input_data['puntos_conductor']
    puntos_b = input_data['puntos_base']
    comentario = input_data['comentario']

    try:
        pasajero = Pasajero.objects.get(user=request.user)
        if pasajero:
            viaje = Viaje.objects.get(id=viaje_id)
    except Pasajero.DoesNotExist:
        return Response({MESSAGE_RESPUESTA: USER_INVALID})
    except Viaje.DoesNotExist:
        return Response({MESSAGE_RESPUESTA: ERROR_VIAJE_ACTIVO})

    try:
        conductor = Conductor.objects.get(dni=dni_conductor)
        if conductor:
            conductor.reputacion += puntos_c
            conductor.viajes_realizados += 1
            conductor.save()
    except Conductor.DoesNotExist:
        return Response({MESSAGE_RESPUESTA: USER_INVALID})

    Calificacion.objects.create(conductor=conductor, pasajero=pasajero, viaje=viaje,
                                puntos_conductor=puntos_c, puntos_base=puntos_b,
                                comentario=comentario)

    viaje.estado = 'F'
    viaje.fecha_hora_fin = datetime.now()
    viaje.save()

    serializer = PedidoSerializer(viaje)

    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "pedidos", {"type": "chat_message", "message": serializer.data})
    async_to_sync(channel_layer.group_send)("chat", {
        "type": "chat.nuevoviaje", "room_id": pasajero.pk, "username": "", "message": serializer.data})

    return Response({MESSAGE_RESPUESTA: FIN_VIAJE})


class EnviarPosicion(BaseViajesView):
    def post(self, request, *args, **kwargs):
        input_data = request.data.copy()
        pos = ''
        if 'posicion' in input_data:
            pos = input_data['posicion']

        try:
            conductor = Conductor.objects.get(user=request.user)
            viaje = Viaje.objects.get(conductor=conductor, fecha_hora_fin=None)
        except Conductor.DoesNotExist:
            return Response({MESSAGE_RESPUESTA: USER_INVALID})
        except Viaje.DoesNotExist:
            # return Response({MESSAGE_RESPUESTA: ERROR_NO_VIAJE})
            viaje = None
            pass

        if viaje:
            message = {MESSAGE_TYPE: UBICACION_CONDUCTOR, 'posicion': pos}
            pasajero = viaje.pasajero

            try:
                result = push_service.notify_single_device(
                    registration_id=pasajero.firebase_token, data_message=message)
            except FCMError as e:
                return self.respond_error(e)

            self.resultado_solicitud(result, pasajero)

        async_to_sync(get_channel_layer().group_send)("ubicaciones", {
            "type": "pos.message", "message": {'registro': conductor.registro, 'ubicacion': pos}})

        return Response({MESSAGE_RESPUESTA: ENVIA_POSICION_OK})


class ViajeDetail(generics.RetrieveAPIView):
    queryset = Viaje.objects.all()
    serializer_class = ViajeSerializer


@login_required
def PedidosList(request):
    time_threshold = datetime.now() - timedelta(hours=24)
    viajes = Viaje.objects.filter(Q(operador=request.user) | Q(operador=None)).filter(
        fecha_hora__gte=time_threshold).order_by('-fecha_hora')

    viajes_data = viajes.exclude(estado='F')

    serializer = PedidoSerializer(viajes_data, many=True)

    context = {
        'room_name_json': mark_safe(json.dumps('pedidos')),
        'viajes_json': mark_safe(json.dumps(serializer.data))
    }

    context['viajes'] = viajes_data
    context['encurso'] = viajes.filter(estado='E')
    context['completados'] = viajes.filter(estado='O')
    context['abordo'] = viajes.filter(estado='B')
    context['conductores'] = Conductor.objects.filter(estado='a')
    context['zonas'] = Zona.objects.filter(activated=True).order_by()

    return render(request, 'pedidos2.html', context)


# si tiene que cambiar el estado a completado el campo conductor estara vacio
# si tiene que editar el conductor la bandera debe estar en falso y no debe cambiar el estado


def cambiarEstado(request):
    if request.is_ajax():
        viaje = request.GET.get('id_viaje', None)
        conductor = request.GET.get('id_conductor', None)
        flag = request.GET.get('flag', None)
        v = Viaje.objects.get(pk=viaje)
        datos = {}
        try:
            c = Conductor.objects.get(pk=conductor)
            z = c.zona
            cont = Conductor.objects.filter(zona=z).count()
            datos['contador'] = cont - 1
            c.zona = None
            if z is not None:
                datos['zona'] = z.id
                c.ultima_ubicacion = Point(
                    z.base.coords[1], z.base.coords[0], srid=4326)
            v.conductor = c
            v.operador = request.user
            c.save()
            datos['dni'] = c.dni
        except Exception as e:
            raise Http404("No hay conductores")

        if flag == "true":
            if v.estado == 'P':
                v.estado = 'E'
            elif v.estado == 'E':
                v.estado = 'O'
            else:
                v.estado = 'F'
                v.fecha_hora_inicio = datetime.now()
                v.fecha_hora_fin = datetime.now()
        v.save()

        asignarMovil(request, v)

        datos['estado'] = v.estado
        datos['conductor'] = v.conductor.registro
        datos['id'] = v.id
        datos['nombre_pasajero'] = str(v.pasajero)
        datos['cel_pasajero'] = v.pasajero.telephone
        datos['movil'] = v.conductor.registro

        data = json.dumps(datos)

        async_to_sync(get_channel_layer().group_send)(
            "pedidos", {"type": "chat_message", "message": data})
        async_to_sync(get_channel_layer().group_send)("chat", {
            "type": "chat.nuevoviaje", "room_id": v.pasajero.pk, "username": "", "message": data})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def asignarMovil(request, viaje):
    """
    Vista para asignaicón manual de movil en solicitud de viaje

    """
    if not viaje.pasajero.user:
        return

    tarifa = ''
    if viaje.destino is not None:
        try:
            origen = str(viaje.origen.coords[0]) + \
                ',' + str(viaje.origen.coords[1])
            destino = str(viaje.destino.coords[0]) + \
                ',' + str(viaje.destino.coords[1])
            tarifa = CalcularTarifa(origen, destino)
        except Exception as e:
            log.exception(u'%s; %s', e, str(e))

    serializer = ViajeSerializer(viaje)
    movil = Movilog.objects.filter(conductor=viaje.conductor)
    movil_s = 'S/M'
    if movil:
        movilSer = MovilSerializer(movil[0])
        movil_s = movilSer.data

    data_message = {
        MESSAGE_TYPE: MESSAGE_TYPE_REQUEST_ACCEPTED,
        'viaje': serializer.data,
        'tarifa': tarifa,
        'movil': movil_s
    }

    registration_ids = []
    registration_ids.append(viaje.pasajero.firebase_token)

    if viaje.conductor.firebase_token is not None:
        registration_ids.append(viaje.conductor.firebase_token)

    try:
        result = push_service.notify_multiple_devices(
            registration_ids=registration_ids, data_message=data_message)
    except FCMError as e:
        log.exception(u'%s; %s', e, str(e))
# END asignar_movil


def cancelarPedido(request):
    if request.is_ajax():
        mimetype = 'application/json'

        viaje = request.GET.get('id_viaje', None)
        v = Viaje.objects.get(pk=viaje)

        try:
            c = Conductor.objects.get(dni=v.conductor.dni)
            c.zona = v.zona
            c.hora_asignacion = datetime.strptime(
                '01/01/2010 10:00', '%d/%m/%Y %H:%M')
            c.save()
        except Exception as e:
            log.exception(u'%s; %s', e, str(e))

        v.estado = 'C'
        v.fecha_hora_fin = FECHA_CANCEL
        v.save()

        data = {
            MESSAGE_TYPE: VIAJE_CANCELADO_BASE
        }

        if v.pasajero and v.pasajero.firebase_token is not None:
            try:
                push_service.notify_single_device(
                    registration_id=v.pasajero.firebase_token, data_message=data)
            except FCMError as e:
                log.exception(u'%s; %s', e, str(e))

        serializer = PedidoSerializer(v)

        async_to_sync(get_channel_layer().group_send)(
            "pedidos", {"type": "chat_message", "message": serializer.data})
        async_to_sync(get_channel_layer().group_send)("chat", {
            "type": "chat.nuevoviaje", "room_id": v.pasajero.pk, "username": "", "message": serializer.data})

        data = json.dumps(v.get_estado_display())
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def asignarZona(request):
    if request.is_ajax():
        zona = request.GET.get('id_zona', None)
        conductor = request.GET.get('id_conductor', None)
        datos = {}
        z = Zona.objects.get(pk=zona)
        c = Conductor.objects.get(pk=conductor)
        if c.zona is not None:
            zonaAnt = c.zona
            datos['zonaAnt'] = zonaAnt.id
            datos['contAnterior'] = Conductor.objects.filter(
                zona=zonaAnt).count() - 1
        c.zona = z
        c.hora_asignacion = datetime.now()
        c.save()
        es = c.esAbonado()
        # print(es)

        datos['contador'] = Conductor.objects.filter(zona=z).count()
        datos['dni'] = c.dni
        datos['conductor'] = c.registro
        datos['zona'] = z.id
        datos['zname'] = z.nombre
        datos['esAbonado'] = c.esAbonado()
        datos['tipo'] = "add_zona"
        data = mark_safe(json.dumps(datos))

        async_to_sync(get_channel_layer().group_send)(
            "pedidos", {"type": "chat_message", "message": data})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def editarPedido(request):
    if request.is_ajax():
        viaje = request.GET.get('id_viaje', None)
        v = Viaje.objects.get(pk=viaje)

        from conductores.serializers import ConductorSerializer

        context = {}
        if v.zona is not None:
            context['conductores'] = ConductorSerializer(
                v.zona.obtenerConductores2(), many=True).data
        else:
            context['conductores'] = ConductorSerializer(
                Conductor.objects.all(), many=True).data
        if v.zona is not None:
            context['zona'] = v.zona.id
        data = mark_safe(json.dumps(context))

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def editarZona(request):
    if request.is_ajax():

        context = {}

        context['zonas'] = ZonaSerializer(
            Zona.objects.filter(activated=True), many=True).data

        data = mark_safe(json.dumps(context))

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def cambiarZona(request):
    if request.is_ajax():
        viaje = request.GET.get('id_viaje', None)
        v = Viaje.objects.get(pk=viaje)
        zona = request.GET.get('id_zona', None)
        print(zona)
        context = {}

        from conductores.serializers import ConductorSerializer

        if zona is not '0':
            z = Zona.objects.get(pk=zona)
            v.zona = z
            context['zona'] = v.zona.id
            context['nombrezona'] = v.zona.nombre
            context['conductores'] = ConductorSerializer(
                v.zona.obtenerConductores2(), many=True).data
        else:
            v.zona = None
            context['zona'] = None
            context['nombrezona'] = 'Sin zona'
            context['conductores'] = ConductorSerializer(
                Conductor.objects.all(), many=True).data

        context['viaje'] = viaje
        context['tipo'] = "edit_zona"
        data = json.dumps(context)

        async_to_sync(get_channel_layer().group_send)(
            "pedidos", {"type": "chat_message", "message": data})

        v.save()

        data = mark_safe(json.dumps(context))

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def quitarZona(request):
    if request.is_ajax():
        conductor = request.GET.get('id_conductor', None)
        c = Conductor.objects.get(pk=conductor)
        z = c.zona
        c.zona = None
        c.hora_asignacion = datetime.now()
        c.save()
        datos = {}
        datos['contador'] = Conductor.objects.filter(zona=z).count()
        datos['zona'] = z.id
        datos['zname'] = z.nombre
        datos['conductor'] = c.registro
        datos['dni'] = c.dni
        datos['tipo'] = "rm_zona"
        data = json.dumps(datos)

        async_to_sync(get_channel_layer().group_send)(
            "pedidos", {"type": "chat_message", "message": data})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


def cambiar_fechaAsignacion(request):
    if request.is_ajax():
        conductor1 = request.GET.get('id_1', None)
        conductor2 = request.GET.get('id_2', None)
        c1 = Conductor.objects.get(pk=conductor1)
        c2 = Conductor.objects.get(pk=conductor2)
        fecha2 = c2.hora_asignacion
        c2.hora_asignacion = c1.hora_asignacion
        c1.hora_asignacion = fecha2
        c1.save()
        c2.save()
        datos = {}

        from conductores.serializers import ConductorSerializer

        datos['conductores_zona'] = ConductorSerializer(
            c1.zona.obtenerConductores2(), many=True).data
        datos['zona'] = c1.zona.pk
        datos['tipo'] = "chng_zona"
        data = json.dumps(datos)

        async_to_sync(get_channel_layer().group_send)(
            "pedidos", {"type": "chat_message", "message": data})

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


# Listar los pedidos finalizados
class PedidosFinalizados(LoginRequiredMixin, ListView):
    model = Viaje
    template_name = 'pedidosFinalizados.html'
    context_object_name = "viajes"

    def get_queryset(self):
        n = Viaje.objects.filter(estado='F').order_by('-fecha_hora')
        return n

    def get_context_data(self, **kwargs):
        context = super(PedidosFinalizados, self).get_context_data(**kwargs)
        return context


# Este metodo es para cargar los viajes desde la pagina del chat
# Es la plataforma que tiene que ver el operador telefonico
def pedidoChat(request):
    if request.is_ajax():
        pasajero = request.GET.get('pasajero', None)
        origen = request.GET.get('origen', None)
        destino = request.GET.get('destino', None)
        tipo = request.GET.get('tipo', None)
        telefono = request.GET.get('telefono', None)
        comentario = request.GET.get('comentario', None)
        fecha = request.GET.get('fecha', None)
        origenLat = request.GET.get('origenLat', None)
        origenLong = request.GET.get('origenLong', None)
        destinoLat = request.GET.get('destinoLat', None)
        destinoLong = request.GET.get('destinoLong', None)
        date_object = datetime.strptime(fecha, '%d/%m/%Y %H:%M')

        from django.contrib.gis.geos import Point

        try:
            fecha_hora = datetime.now()
            zona = None
            ori = Point(0, 0)
            if origenLat != '':
                ori = Point(float(origenLat), float(origenLong))
                # Se crea otra variable xq django toma al reves la lat y long
                ori_django = Point(float(origenLong), float(origenLat))
                zona = Zona.objects.get(activated=True, poli__intersects=ori_django,
                                        hora_desde__lte=fecha_hora.time(), hora_hasta__gte=fecha_hora.time())

            origen = origen.split(',')[0]
        except Zona.DoesNotExist:
            zona = None
        except Exception as e:
            log.exception(u'%s; %s', e, str(e))

        try:
            des = Point(0, 0)
            if destino is not None and destino != '':
                des = Point(float(destinoLat), float(destinoLong))
                destino = destino.split(',')[0]
        except Exception as e:
            log.exception(u'%s; %s', e, str(e))

        try:
            nuevoPasajero = Pasajero.objects.get(dni="123456789")
            
            nuevoPedido = Viaje.objects.create(
                conductor=None,
                origen=ori,
                destino=des,
                pasajero=nuevoPasajero,
                fecha_hora_fin=None,
                origen_descripcion=origen,
                destino_descripcion=destino,
                fecha_hora=date_object,
                fecha_hora_inicio=None,
                zona=zona,
                tipo=tipo,
                operador=None,
                receptor=None,
                comentario=pasajero,
                estado='P')
        except Exception as e:
            log.exception(u'%s; %s', e, str(e))
            raise Http404

        serializer = PedidoSerializer(nuevoPedido)

        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            "pedidos", {"type": "chat_message", "message": serializer.data})
        async_to_sync(channel_layer.group_send)("chat", {
            "type": "chat.nuevoviaje", "room_id": nuevoPasajero.pk, "username": "", "message": serializer.data})

        data = json.dumps(serializer.data)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404


class DetallePedido(DetailView):
    model = Viaje
    template_name = "detallePedido.html"

    def get_context_data(self, **kwargs):
        context = super(DetallePedido, self).get_context_data(**kwargs)
        viaje = Viaje.objects.get(id=self.kwargs['pk'])
        serializer = PedidoSerializer(viaje)
        context['pedido'] = mark_safe(json.dumps(serializer.data))
        return context

# Este metodo es para listar los viajes junto con el formulario para cargar un viaje nuevo
# Esta panatalla la tiene que ver el que atiende el telefono y carga nos pedidos
@login_required
def NuevoViajeList(request):
    time_threshold = datetime.now() - timedelta(hours=24)
    viajes_data = Viaje.objects.filter(fecha_hora__gte=time_threshold).exclude(
        estado='F').order_by('-fecha_hora')

    serializer = PedidoSerializer(viajes_data, many=True)

    context = {
        'room_name_json': mark_safe(json.dumps('pedidos')),
        'viajes_json': mark_safe(json.dumps(serializer.data))
    }
    context['viajes'] = viajes_data
    context['encurso'] = Viaje.objects.filter(
        fecha_hora__gte=time_threshold, estado='E', operador=request.user).order_by('-fecha_hora')
    context['completados'] = Viaje.objects.filter(
        fecha_hora__gte=time_threshold, estado='O', operador=request.user).order_by('-fecha_hora')
    context['abordo'] = Viaje.objects.filter(
        fecha_hora__gte=time_threshold, estado='B', operador=request.user).order_by('-fecha_hora')
    return render(request, 'cargaViaje.html', context)
