from __future__ import absolute_import, unicode_literals
from celery import shared_task
from time import sleep
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from .models import Viaje
from .serializers import PedidoSerializer
import logging

l = logging.getLogger(__name__)
# El decorador shared_task sirve para crear tareas independientes a la app.
# La tarea solo es una simulación tonta.
# Pero sabed que podéis usar cualquier librería y clase aquí. Incluido el orm para acceder a la bd.
@shared_task
def activar_viaje(id):
    print('viaje activando :' + id)

    try:
        viaje = Viaje.objects.get(id=id, estado='P')
        serializer = PedidoSerializer(viaje)         
                    
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)("pedidos", {"type": "chat_message", "message":serializer.data })        
        async_to_sync(channel_layer.group_send)("chat", {"type": "chat.nuevoviaje", "room_id": viaje.pasajero.pk, "username":"", "message": serializer.data})
        l.info('viaje activado: ' + id)
        
    except Viaje.DoesNotExist as e:
        l.exception(u'%s; %s', e, str(e))
        print('error, viaje no existe. ' + id)