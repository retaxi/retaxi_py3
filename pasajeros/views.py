from .serializers import PasajeroSerializer
from rest_framework import generics, mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Pasajero
from datetime import datetime

class PasajeroDetail(generics.DestroyAPIView,
                     generics.UpdateAPIView,
                     generics.RetrieveAPIView):
    queryset = Pasajero.objects.all()
    serializer_class = PasajeroSerializer


class PasajeroList(generics.ListAPIView):
    queryset = Pasajero.objects.all()
    serializer_class = PasajeroSerializer


@api_view(['POST'])
def pasajero_msg_token(request):
    try:
        pasajero = Pasajero.objects.get(user=request.user)
        data = request.data.copy()
        token = data['token']
        pasajero.firebase_token = token
        pasajero.save()
		
    except Pasajero.DoesNotExist:
        return Response({'respuesta': 'ERROR'})
    return Response({'respuesta': 'OK'})


@api_view(['POST'])
def pasajero_data_udt(request):
    try:
        pasajero = Pasajero.objects.get(user=request.user)
        data = request.data.copy()
        pasajero.birthday =  datetime.strptime(data['birthday'], "%d/%m/%Y")
        pasajero.sex = data['sex']
        pasajero.telephone = data['telephone']
        pasajero.save()
		
    except Pasajero.DoesNotExist:
        return Response({'respuesta': 'ERROR'})
    return Response({'respuesta': 'OK'})