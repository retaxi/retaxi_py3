from django.conf import settings
from django.contrib.gis.db import models


class Pasajero(models.Model):
    dni = models.CharField(blank=True, null=True, verbose_name='dni',max_length=50,)
    first_name = models.CharField(verbose_name='nombre', max_length=50, blank=False, null=False)
    last_name = models.CharField(verbose_name='apellido', max_length=50, blank=True, null=True)
    birthday = models.DateField(verbose_name='fecha de nacimiento', blank=True, null=True)
    telephone = models.CharField(verbose_name='telefono', max_length=20, blank=True, null=True)
    email = models.CharField(verbose_name='email', max_length=100, blank=True, null=True)
    
    SEXO = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
    )
    sex = models.CharField(max_length=1, choices=SEXO, blank=True, null=True)
    last_location = models.PointField(null=True, blank=True, default='POINT(0 0)', srid=4326)
    reputacion= models.FloatField(blank=True, null=True, default=1)
    viajes_realizados = models.IntegerField(blank=True, null=True, default=0)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    firebase_token = models.TextField(verbose_name='firebase_token', blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)