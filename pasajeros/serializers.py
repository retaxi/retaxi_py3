from .models import Pasajero
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from users.models import Userthumb

class PasajeroSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """

    class Meta:
        model = Pasajero
        geo_field = "last_location"
    
        fields = (
            'first_name', 
            'last_name',
            'telephone',
            'email',
            'birthday',
            'sex',
            'reputacion',
            'viajes_realizados',
            'firebase_token', )

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.  

class UserPasajeroSerializer(serializers.ModelSerializer):    
    social_thumb = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()
    def get_social_thumb(self, obj):
        try:
            userthumb = Userthumb.objects.get(user=obj.user)        
        except Userthumb.DoesNotExist:
            return ''

        return userthumb.social_thumb

    def get_token(self, obj):
        token, created = Token.objects.get_or_create(user=obj.user)
        return token.key

    class Meta:
        model = Pasajero
        geo_field = "last_location"
    
        fields = (
            'dni',
            'first_name', 
            'last_name',
            'telephone',
            'email',
            'birthday',
            'sex',
            'reputacion',
            'viajes_realizados',
            'social_thumb',
            'token',
             )