from django.contrib import admin
from .models import Pasajero

class PasajeroAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'dni', 'telephone',]
    search_fields = ['first_name', 'last_name','dni',]
    
    class Meta:
        model = Pasajero


admin.site.register(Pasajero, PasajeroAdmin)
